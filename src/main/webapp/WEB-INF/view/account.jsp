<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n.frontend" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/> - <fmt:message key="account"/></title>
    </head>
    <body>
        <h1><fmt:message key="account"/></h1>
        <p><a href="withdraw-bitcoin.html"><fmt:message key="withdraw-bitcoin"/></a></p>
        <h2><fmt:message key="welcome"/> ${trader.openId}</h2>
        <p><fmt:message key="bitcoin-balance"/>: ${bitcoinBalance}</p>
        <h2><fmt:message key="open-buy-orders"/></h2>
        <table border="1">
            <tr>
                <th><fmt:message key="order-placed"/></th>
                <th><fmt:message key="bitcoin-price"/></th>
                <th><fmt:message key="bitcoin-amount"/></th>
                <th><fmt:message key="total"/></th>
            </tr>
            <c:forEach items="${buyOrders}" var="order">
                <tr>
                    <td>${order.placed}</td>
                    <td>${order.price}</td>
                    <td>${order.amount}</td>
                    <td>${order.total}</td>
                </tr>
            </c:forEach>
        </table>

        <h2><fmt:message key="open-sell-orders"/></h2>
        <table border="1">
            <tr>
                <th><fmt:message key="order-placed"/></th>
                <th><fmt:message key="bitcoin-price"/></th>
                <th><fmt:message key="bitcoin-amount"/></th>
                <th><fmt:message key="total"/></th>
            </tr>
            <c:forEach items="${sellOrders}" var="order">
                <tr>
                    <td>${order.placed}</td>
                    <td>${order.price}</td>
                    <td>${order.amount}</td>
                    <td>${order.total}</td>
                </tr>
            </c:forEach>
        </table>
        <h2><fmt:message key="buy-trades"/></h2>
        <table border="1">
            <tr>
                <th><fmt:message key="traded-buy"/></th>
                <th><fmt:message key="bitcoin-price"/></th>
                <th><fmt:message key="bitcoin-amount"/></th>
                <th><fmt:message key="total"/></th>
            </tr>
            <c:forEach items="${buyTrades}" var="trade">
                <tr>
                    <td>${trade.traded}</td>
                    <td>${trade.price}</td>
                    <td>${trade.amount}</td>
                    <td>${trade.total}</td>
                </tr>
            </c:forEach>
        </table>
        <h2><fmt:message key="sell-trades"/></h2>
        <p>${sellTrades}</p>
        <h2><fmt:message key="bitcoin-transactions"/></h2>
        <table border="1">
            <tr>
                <th><fmt:message key="timestamp"/></th>
                <th><fmt:message key="bitcoin-amount"/></th>
                <th><fmt:message key="bitcoin-address"/></th>
                <th><fmt:message key="status"/></th>
                <th><fmt:message key="details"/></th>
            </tr>
            <c:forEach items="${bitcoinTransactions}" var="transaction">
                <tr>
                    <td>${transaction.placed}</td>
                    <td>${transaction.amount}</td>
                    <td>${transaction.address}</td>
                    <td>${transaction.status}</td>
                    <td><a href="http://blockexplorer.com/tx/${transaction.transactionHash}"><fmt:message key="details"/></a></td>
                </tr>
            </c:forEach>
        </table>
        <h2><fmt:message key="bank-transactions"/></h2>
        <p>${bankTransactions}</p>
    </body>
</html>
