<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n.frontend" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
        <h1><fmt:message key="title"/></h1>
        <a href="buy.html"><fmt:message key="buy-bitcoin"/></a>
        <a href="sell.html"><fmt:message key="sell-bitcoin"/></a>
        <a href="account.html"><fmt:message key="account"/></a>
    </body>
</html>
