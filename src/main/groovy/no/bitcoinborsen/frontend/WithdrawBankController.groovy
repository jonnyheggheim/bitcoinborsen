package no.bitcoinborsen.frontend

import groovy.util.logging.Slf4j
import java.security.Principal
import no.bitcoinborsen.backend.service.BankService
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.frontend.forms.WithdrawForm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import no.bitcoinborsen.model.*

@Slf4j
@Controller
@RequestMapping("/withdraw-bank.html")
final class WithdrawBankController {
    @Autowired
    BankService bankService
    @Autowired
    DateGenerator dateGenerator
    @Autowired
    IdGenerator idGenerator
    @Autowired
    TraderService traderService

    @RequestMapping(method = RequestMethod.POST)
    String create(WithdrawForm form, Principal principal) {
        log.info("New withdraw: {}", form)
        Trader trader = traderService.getTraderFromOpenId(principal.getName())
        def withdraw = new BankTransaction(idGenerator.random(), trader.id, form.amount.negate(), form.address,
                dateGenerator.now(), '000000000000000000000', TransactionStatus.QUEUE)

        bankService.newWithdraw(withdraw)
        'redirect:index.html'
    }

    @RequestMapping(method = RequestMethod.GET)
    String getView(Model model) {
        model.addAttribute(new WithdrawForm())
        'withdraw-bank'
    }
}
