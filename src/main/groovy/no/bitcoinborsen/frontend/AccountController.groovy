package no.bitcoinborsen.frontend

import java.security.Principal
import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.model.Trader
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import no.bitcoinborsen.backend.service.*
import org.springframework.beans.factory.annotation.Qualifier

@Controller
@RequestMapping("/account.html")
final class AccountController {
    @Bitcoin
    @Autowired
    TransactionService bitcoinService

    @Bitcoin
    @Autowired
    BalanceService bitcoinBalanceService

    @Autowired
    @Qualifier('buy')
    OrderService buyOrderService

    @Autowired
    @Qualifier('sell')
    OrderService sellOrderService

    @Autowired
    TraderService traderService
    @Autowired
    TradeService tradeService

    @RequestMapping(method = RequestMethod.GET)
    String getView(Model model, Principal principal) {
        Trader trader = traderService.getTraderFromOpenId(principal.getName())

        def buyOrders = buyOrderService.getOrdersForTrader(trader.id)
        def sellOrders = sellOrderService.getOrdersForTrader(trader.id)
        def buyTrades = tradeService.getBuysForTrader(trader.id)
        def sellTrades = tradeService.getSalesForTrader(trader.id)
        def bitcoinBalance = bitcoinBalanceService.getBalance(trader.id)
        def bitcoinTransactions = bitcoinService.getTransactionsForTrader(trader.id)

        model.addAttribute("trader", trader)
        model.addAttribute("buyOrders", buyOrders)
        model.addAttribute("sellOrders", sellOrders)
        model.addAttribute("buyTrades", buyTrades)
        model.addAttribute("sellTrades", sellTrades)
        model.addAttribute("bitcoinBalance", bitcoinBalance)
        model.addAttribute("bitcoinTransactions", bitcoinTransactions)

        'account'
    }
}
