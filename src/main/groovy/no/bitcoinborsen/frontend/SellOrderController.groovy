package no.bitcoinborsen.frontend

import groovy.util.logging.Slf4j
import java.security.Principal
import no.bitcoinborsen.backend.service.OrderService
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.frontend.forms.NewOrderForm
import no.bitcoinborsen.model.SellOrder
import no.bitcoinborsen.model.Trader
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@Slf4j
@Controller
@RequestMapping('/sell.html')
final class SellOrderController {
    @Autowired
    @Qualifier('sell')
    OrderService orderService

    @Autowired
    DateGenerator dateGenerator
    @Autowired
    IdGenerator idGenerator
    @Autowired
    TraderService traderService

    @RequestMapping(method = RequestMethod.POST)
    String create(NewOrderForm form, Principal principal) {
        log.info("New sell order: {}", form)
        Trader trader = traderService.getTraderFromOpenId(principal.name)
        SellOrder order = new SellOrder(form.price,
                form.amount,
                trader.id,
                dateGenerator.now(),
                idGenerator.random())

        orderService.newOrder(order)

        'redirect:/account.html'
    }

    @RequestMapping(method = RequestMethod.GET)
    String getView(Model model) {
        model.addAttribute(new NewOrderForm())

        'sell'
    }
}
