package no.bitcoinborsen.frontend.admin

import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.backend.service.TransactionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.request.WebRequest

@Controller
@RequestMapping('/admin/confirm-withdraw.html')
final class ConfirmWithdrawController {
    @Bitcoin
    @Autowired
    TransactionService bitcoinService

    @RequestMapping(method = RequestMethod.GET)
    String index(Model model) {
        def withdrawals = bitcoinService.getQueuedWithdrawals()
        model.addAttribute('withdrawals', withdrawals)

        'admin/confirm-withdraw'
    }

    @RequestMapping(method = RequestMethod.POST)
    String confirm(WebRequest request) {
        UUID id = UUID.fromString(request.getParameter('id'))
        bitcoinService.confirmWithdrawal(id)

        'redirect:/admin/confirm-withdraw.html'
    }
}
