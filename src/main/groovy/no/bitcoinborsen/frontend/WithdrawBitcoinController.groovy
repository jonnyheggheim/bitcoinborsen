package no.bitcoinborsen.frontend

import groovy.util.logging.Slf4j
import java.security.Principal
import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.service.TransactionService
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.frontend.forms.WithdrawForm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import no.bitcoinborsen.model.*

@Slf4j
@Controller
@RequestMapping("/withdraw-bitcoin.html")
final class WithdrawBitcoinController {
    @Bitcoin
    @Autowired
    TransactionService bitcoinService

    @Autowired
    DateGenerator dateGenerator
    @Autowired
    IdGenerator idGenerator
    @Autowired
    TraderService traderService

    @RequestMapping(method = RequestMethod.POST)
    String create(WithdrawForm form, Principal principal) {
        log.info("New withdraw: {}", form)
        Trader trader = traderService.getTraderFromOpenId(principal.name)
        BitcoinTransaction withdraw = new BitcoinTransaction(idGenerator.random(), trader.id,
                form.amount.negate(), form.address, dateGenerator.now(), "", TransactionStatus.QUEUE)

        bitcoinService.newWithdraw(withdraw)

        'redirect:index.html'
    }

    @RequestMapping(method = RequestMethod.GET)
    String getView(Model model) {
        model.addAttribute(new WithdrawForm())

        'withdraw-bitcoin'
    }
}
