package no.bitcoinborsen.frontend

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
final class IndexController {
    @RequestMapping("/index.html")
    String index() {
        'index'
    }
}
