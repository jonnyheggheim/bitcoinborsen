package no.bitcoinborsen.frontend

import java.security.Principal
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.model.Trader
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@Controller
@RequestMapping("/deposit-bitcoin.html")
final class DepositBitcoinController {
    @Autowired
    TraderService traderService

    @RequestMapping(method = RequestMethod.GET)
    String getView(Model model, Principal principal) {
        Trader trader = traderService.getTraderFromOpenId(principal.name)
        model.addAttribute('trader', trader)

        'deposit-bitcoin'
    }
}
