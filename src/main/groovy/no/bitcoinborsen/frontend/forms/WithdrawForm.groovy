package no.bitcoinborsen.frontend.forms

import groovy.transform.Canonical

@Canonical
class WithdrawForm {
    BigDecimal amount
    String address
}
