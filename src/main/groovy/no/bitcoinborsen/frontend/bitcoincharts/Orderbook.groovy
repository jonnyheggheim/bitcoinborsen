package no.bitcoinborsen.frontend.bitcoincharts

import groovy.json.StreamingJsonBuilder
import no.bitcoinborsen.backend.service.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@Controller
@RequestMapping('/bitcoincharts/orderbook.json')
final class Orderbook {
    @Autowired
    @Qualifier('buy')
    OrderService buyOrderService

    @Autowired
    @Qualifier('sell')
    OrderService sellOrderService

    @RequestMapping(method = RequestMethod.GET, produces = 'application/json')
    void list(Writer response) {
        def json = new StreamingJsonBuilder(response)
        json {
            asks sellOrderService.listOrders().collect {order -> [order.price, order.amount]}
            bids buyOrderService.listOrders().collect {order -> [order.price, order.amount]}
        }
    }
}
