package no.bitcoinborsen.backend.task

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.ocr.OcrParser
import no.bitcoinborsen.backend.ocr.OcrTransaction
import no.bitcoinborsen.backend.service.BankService
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.util.IdGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import com.jcraft.jsch.*
import no.bitcoinborsen.model.*

@Slf4j
@Component
final class CheckforNewBankDeposits {
    private knownHosts = '91.102.24.145 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQCC6Ra6vmvGeeP0CRW75BLgE0H/vV0Z5DviFhB3F/glAdNquN1mTWl2su48T2pkcHHW2K7euAUVEZ1DZu5XnFX+FeRIHK2t9ErAFx9B56MYxUQkwToFSoRAknUj0A7v1GcmTGmgDtTrIlxupoKxjQl34IFwQGTLAEXm8iexdbRNbQ=='
    private username = 'E159989'
    private address = '91.102.24.145'
    private port = 22
    private directory = '/Outbound'

    @Value('${nets.sftp.privateKey}')
    String privateKeyLocation
    @Autowired
    BankService bankService
    @Autowired
    OcrParser ocrParser
    @Autowired
    IdGenerator idGenerator
    @Autowired
    TraderService traderService

    @Scheduled(fixedDelay = 60000L)
    void pollSftpServer() {
        JSch ssh = new JSch()

        ssh.setKnownHosts(new ByteArrayInputStream(knownHosts.bytes))
        ssh.addIdentity(privateKeyLocation)
        Session session = ssh.getSession(username, address, port)

        session.connect()
        ChannelSftp sftp = session.openChannel("sftp")
        sftp.connect()
        log.debug("Connected ${address}")

        sftp.cd(directory)
        sftp.ls('*').each() {
            log.debug("Loading: ${it.filename}")
            def input = sftp.get(it.filename)
            appendTransactions(input)
        }
    }

    private void appendTransactions(InputStream input) {
        OcrTransaction[] transactions = ocrParser.listTransactions(input)
        transactions.each {
            def transaction = fromOcrTransaction(it)
            bankService.appendDeposit(transaction)
        }
    }

    private BankTransaction fromOcrTransaction(OcrTransaction transaction) {
        Trader trader = traderService.getFromKidNumber(transaction.kid)
        new BankTransaction(idGenerator.random(), trader.id, transaction.amount, transaction.accountNumber,
                transaction.transfered, TransactionStatus.FINISH)
    }
}

