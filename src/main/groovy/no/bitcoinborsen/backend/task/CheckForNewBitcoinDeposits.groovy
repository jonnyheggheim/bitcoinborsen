package no.bitcoinborsen.backend.task

import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.service.TransactionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.paradoxs.bitcoin.client.BitcoinClient
import ru.paradoxs.bitcoin.client.TransactionInfo
import no.bitcoinborsen.backend.util.*
import no.bitcoinborsen.model.*

@Component
final class CheckForNewBitcoinDeposits {
    @Bitcoin
    @Autowired
    TransactionService bitcoinService
    @Autowired
    BitcoinClient bitcoinClient
    @Autowired
    TraderService traderService
    @Autowired
    IdGenerator idGenerator
    @Autowired
    DateGenerator dateGenerator
    @Autowired
    AccountName accountName

    @Scheduled(fixedDelay = 60000L)
    void pollBitcoinClient() {
        traderService.listAllTraders().each { trader ->
            updateDepositsForTrader(trader)
        }
    }

    private void updateDepositsForTrader(Trader trader) {
        String account = accountName.fromOpenId(trader.openId)
        bitcoinClient.listTransactions(account).each { transaction ->
            if (transaction.getCategory().equals("receive")) {
                appendDeposit(transaction, trader)
            }
        }
    }

    private void appendDeposit(TransactionInfo transaction, Trader trader) {
        UUID id = idGenerator.random()
        Date placed = dateGenerator.now()

        BigDecimal amount = transaction.amount
        String hash = transaction.txId
        TransactionStatus status = TransactionStatus.QUEUE

        if (transaction.confirmations >= 2) {
            status = TransactionStatus.FINISH
        }

        def deposit = new BitcoinTransaction(id, trader.id, amount, trader.bitcoinAddress, placed, hash, status)

        bitcoinService.appendDeposit(deposit)
    }
}
