package no.bitcoinborsen.backend.util

import org.springframework.stereotype.Component

@Component
class Mod10BankKid implements BankKid {
    @Override
    String fromId(UUID id) {
        long base = Math.abs(id.getLeastSignificantBits())
        String padded = String.format("%019d", base)
        String reserved = "000"
        String control = calculateControl(padded + reserved)
        return padded + reserved + control
    }

    // From wikipedia: http://en.wikipedia.org/wiki/Luhn_algorithm
    private String calculateControl(String number) {
        def sumTable = [[0, 2, 4, 6, 8, 1, 3, 5, 7, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]
        int sum = 0, flip = 0
        number.reverse().each { digit ->
            sum += sumTable[flip++ & 0x1][digit.toInteger()]
        }
        int modulusResult = (sum % 10)

        modulusResult == 0 ? 0 : 10 - modulusResult
    }
}
