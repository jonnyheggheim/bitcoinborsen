package no.bitcoinborsen.backend.util

interface DateGenerator {
    Date now()
}
