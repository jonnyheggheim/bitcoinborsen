package no.bitcoinborsen.backend.util

import org.springframework.stereotype.Component

@Component
final class DefaultDateGenerator implements DateGenerator {

    @Override
    Date now() {
        new Date()
    }
}
