package no.bitcoinborsen.backend.util

import org.springframework.stereotype.Component

@Component
final class PrefixAccountName implements AccountName {

    @Override
    String fromOpenId(String openId) {
        "bitcoinborsen::trader::" + openId
    }
}
