package no.bitcoinborsen.backend.util

import org.springframework.stereotype.Component

@Component
final class DefaultIdGenerator implements IdGenerator {
    @Override
    UUID random() {
        UUID.randomUUID()
    }
}
