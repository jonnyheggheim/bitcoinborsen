package no.bitcoinborsen.backend.util

interface BankKid {
    String fromId(UUID id)
}
