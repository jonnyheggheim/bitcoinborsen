package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.TraderDao
import no.bitcoinborsen.backend.dao.TraderNotFoundException
import no.bitcoinborsen.backend.util.AccountName
import no.bitcoinborsen.backend.util.BankKid
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.model.Trader
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.paradoxs.bitcoin.client.BitcoinClient

@Service
final class TraderServiceImpl implements TraderService {
    @Autowired
    TraderDao traderDao
    @Autowired
    IdGenerator idGenerator
    @Autowired
    BitcoinClient bitcoinClient
    @Autowired
    AccountName accountName
    @Autowired
    BankKid bankKid

    @Override
    void haveBeenAuthenticated(String username, String email) {
        try {
            traderDao.getTraderByOpenId(username)
        } catch (TraderNotFoundException ex) {
            createNewTrader(username, email)
        }
    }

    private void createNewTrader(String username, String email) {
        UUID id = idGenerator.random()
        String account = accountName.fromOpenId(username)
        String address = bitcoinClient.getAccountAddress(account)
        String kid = bankKid.fromId(id)

        Trader newTrader = new Trader(id, username, email, address, kid)

        traderDao.newTrader(newTrader)
    }

    @Override
    Trader getTraderFromOpenId(String openId) {
        traderDao.getTraderByOpenId(openId)
    }

    @Override
    Iterable<Trader> listAllTraders() {
        traderDao.listAllTraders()
    }

    @Override
    Trader getFromKidNumber(String kidNumber) {
        traderDao.getByKidNumber(kidNumber)
    }
}
