package no.bitcoinborsen.backend.service

import no.bitcoinborsen.model.BuyOrder
import no.bitcoinborsen.model.SellOrder

interface OrderService<T> {
    void newOrder(T order)

    void deleteOrder(UUID id)

    List<SellOrder> getOrdersForTrader(UUID id)

    List<BuyOrder> listOrders()
}
