package no.bitcoinborsen.backend.service

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.dao.TradeDao
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.model.Trade
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Slf4j
@Service
final class TradeServiceImpl implements TradeService {
    @Autowired
    TradeDao dao
    @Autowired
    DateGenerator dateGenerator

    @Override
    void newTrade(Trade trade) {
        log.debug("New trade '${trade}'")
        dao.insertTrade(trade)
    }

    @Override
    List<Trade> getBuysForTrader(UUID trader) {
        dao.getBuyesForTrader(trader)
    }

    @Override
    List<Trade> getSalesForTrader(UUID trader) {
        dao.getSalesForTrader(trader)
    }

    @Override
    List<Trade> list(int daysOfTrades) {
        Date since = dateGenerator.now() - daysOfTrades
        dao.list(since)
    }
}
