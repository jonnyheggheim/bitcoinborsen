package no.bitcoinborsen.backend.service

interface BalanceService {
    BigDecimal getSpendable(UUID trader)

    BigDecimal getBalance(UUID trader)
}
