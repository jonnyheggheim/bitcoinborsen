package no.bitcoinborsen.backend.service

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.Bank
import no.bitcoinborsen.backend.dao.BankTransactionDao
import no.bitcoinborsen.backend.dao.TransactionNotFoundException
import no.bitcoinborsen.model.BankTransaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Slf4j
@Service
final class BankServiceImpl implements BankService {
    @Bank
    @Autowired
    BalanceService balanceService

    @Autowired
    BankTransactionDao bankTransactionDao

    @Override
    void newWithdraw(BankTransaction withdraw) {
        assert withdraw != null

        log.info("New bank withdrawal ${withdraw}")
        if (withdraw.amount >= 0.0g) {
            throw new IllegalArgumentException('Amount is positive!')
        }

        BigDecimal spendable = balanceService.getSpendable(withdraw.trader)
        if (spendable < withdraw.amount.negate()) {
            throw new NotEnoughFoundsException()
        }

        bankTransactionDao.insert(withdraw)
    }

    @Override
    void newDeposit(BankTransaction deposit) {
        assert deposit != null

        log.info("New bank deposit ${deposit}")
        if (deposit.amount <= 0.0g) {
            throw new IllegalArgumentException('Amount is negative!')
        }

        bankTransactionDao.insert(deposit)
    }

    @Override
    void appendDeposit(BankTransaction deposit) {
        assert deposit != null

        String transactionId = deposit.transactionId
        try {
            BankTransaction overriding = bankTransactionDao.getByTransactionId(transactionId)
            bankTransactionDao.updateStatus(overriding.id, deposit.status)
        } catch (TransactionNotFoundException ex) {
            newDeposit(deposit)
        }
    }
}
