package no.bitcoinborsen.backend.service

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.Bank
import no.bitcoinborsen.backend.dao.NoOrderFoundException
import no.bitcoinborsen.backend.dao.OrderDao
import no.bitcoinborsen.backend.matcher.OrderMatcher
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import no.bitcoinborsen.model.*

@Slf4j
@Service
@Qualifier('buy')
class BuyOrderService implements OrderService<BuyOrder> {
    @Bank
    @Autowired
    BalanceService bankBalance

    @Autowired
    OrderDao orderDao
    @Autowired
    DateGenerator dateGenerator
    @Autowired
    IdGenerator idGenerator
    @Autowired
    TradeService tradeService

    @Override
    void newOrder(BuyOrder order) {
        log.debug("New buy order '${order}'")

        BigDecimal balance = bankBalance.getSpendable(order.buyer)
        if (balance < order.total) {
            throw new NotEnoughFoundsException()
        }

        try {
            SellOrder sellOrder = orderDao.getLowestSellOrder()
            OrderMatcher matcher = match(sellOrder, order)
            if (matcher.isMatch()) {
                Trade trade = matcher.getTrade()
                orderDao.deleteSellOrder(sellOrder.id)
                tradeService.newTrade(trade)
                if (matcher.hasRestSellOrder()) {
                    orderDao.insertSellOrder(matcher.generateRestSellOrder())
                } else if (matcher.hasRestBuyOrder()) {
                    newOrder(matcher.generateRestBuyOrder())
                }
            } else {
                orderDao.insertBuyOrder(order)
            }
        } catch (NoOrderFoundException ex) {
            orderDao.insertBuyOrder(order)
        }
    }

    @Override
    void deleteOrder(UUID id) {
        log.debug("Delete buy order with id '${id}'")
        orderDao.deleteBuyOrder(id)
    }


    @Override
    List<BuyOrder> getOrdersForTrader(UUID trader) {
        orderDao.getBuyOrdersForTrader(trader)
    }

    @Override
    List<BuyOrder> listOrders() {
        orderDao.listBuyOrders()
    }

    private OrderMatcher match(SellOrder sellOrder, BuyOrder newBuyOrder) {
        OrderMatcher matcher = new OrderMatcher(sellOrder, newBuyOrder)
        matcher.setIdGenerator(idGenerator)
        matcher.setDateGenerator(dateGenerator)

        return matcher
    }
}
