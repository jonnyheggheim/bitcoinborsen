package no.bitcoinborsen.backend.service

import no.bitcoinborsen.model.Trade

interface TradeService {
    void newTrade(Trade trade)

    List<Trade> getBuysForTrader(UUID trader)

    List<Trade> getSalesForTrader(UUID trader)

    List<Trade> list(int daysOfTrades)
}
