package no.bitcoinborsen.backend.service

interface TransactionService<T> {
    void newWithdraw(T withdraw)

    void newDeposit(T deposit)

    List<T> getTransactionsForTrader(UUID trader)

    List<T> getQueuedWithdrawals()

    void confirmWithdrawal(UUID id)

    void appendDeposit(T transaction)
}
