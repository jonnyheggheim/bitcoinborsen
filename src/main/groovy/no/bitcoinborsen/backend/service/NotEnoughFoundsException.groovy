package no.bitcoinborsen.backend.service

import groovy.transform.InheritConstructors

@InheritConstructors
final class NotEnoughFoundsException extends RuntimeException {
    private static final serialVersionUID = 3L
}
