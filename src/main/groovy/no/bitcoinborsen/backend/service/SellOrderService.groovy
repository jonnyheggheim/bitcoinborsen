package no.bitcoinborsen.backend.service

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.backend.dao.NoOrderFoundException
import no.bitcoinborsen.backend.dao.OrderDao
import no.bitcoinborsen.backend.matcher.OrderMatcher
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import no.bitcoinborsen.model.*

@Slf4j
@Service
@Qualifier('sell')
final class SellOrderService implements OrderService<SellOrder> {
    @Bitcoin
    @Autowired
    BalanceService bitcoinBalance

    @Autowired
    OrderDao orderDao
    @Autowired
    DateGenerator dateGenerator
    @Autowired
    IdGenerator idGenerator
    @Autowired
    TradeService tradeService

    @Override
    void newOrder(SellOrder order) {
        log.debug("New sell order '${order}'")

        BigDecimal balance = bitcoinBalance.getSpendable(order.seller)
        if (balance < order.amount) {
            throw new NotEnoughFoundsException()
        }

        try {
            BuyOrder buyOrder = orderDao.getHighestBuyOrder()
            OrderMatcher matcher = match(order, buyOrder)
            if (matcher.isMatch()) {
                Trade trade = matcher.getTrade()
                orderDao.deleteBuyOrder(buyOrder.id)
                tradeService.newTrade(trade)
                if (matcher.hasRestBuyOrder()) {
                    orderDao.insertBuyOrder(matcher.generateRestBuyOrder())
                } else if (matcher.hasRestSellOrder()) {
                    newOrder(matcher.generateRestSellOrder())
                }
            } else {
                orderDao.insertSellOrder(order)
            }
        } catch (NoOrderFoundException ex) {
            orderDao.insertSellOrder(order)
        }
    }

    @Override
    void deleteOrder(UUID id) {
        log.debug("Delete buy order with id '${id}'")
        orderDao.deleteSellOrder(id)
    }

    @Override
    List<SellOrder> getOrdersForTrader(UUID trader) {
        orderDao.getSellOrdersForTrader(trader)
    }

    @Override
    List<SellOrder> listOrders() {
        orderDao.listSellOrders()
    }

    private OrderMatcher match(SellOrder sellOrder, BuyOrder newBuyOrder) {
        OrderMatcher matcher = new OrderMatcher(sellOrder, newBuyOrder)
        matcher.setIdGenerator(idGenerator)
        matcher.setDateGenerator(dateGenerator)

        return matcher
    }
}
