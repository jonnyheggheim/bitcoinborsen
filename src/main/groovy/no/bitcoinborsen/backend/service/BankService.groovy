package no.bitcoinborsen.backend.service

import no.bitcoinborsen.model.BankTransaction

interface BankService {
    void newWithdraw(BankTransaction withdraw)

    void newDeposit(BankTransaction deposit)

    void appendDeposit(BankTransaction deposit)
}
