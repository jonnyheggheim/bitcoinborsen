package no.bitcoinborsen.backend.service

import java.math.RoundingMode
import no.bitcoinborsen.backend.Bank
import no.bitcoinborsen.backend.FatalException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import no.bitcoinborsen.backend.dao.*
import static no.bitcoinborsen.model.TransactionStatus.FINISH

@Bank
@Service
final class BankBalanceService implements BalanceService {
    @Autowired
    BankTransactionDao transactionDao
    @Autowired
    TradeDao tradeDao
    @Autowired
    OrderDao orderDao

    @Override
    BigDecimal getBalance(UUID trader) {
        assert trader != null

        BigDecimal balance = balanceForTrades(trader) + balanceForTransactions(trader)

        return ensurePositiveAmount(balance)
    }

    @Override
    BigDecimal getSpendable(UUID trader) {
        assert trader != null

        BigDecimal spendable = balanceForTrades(trader) +
                spendableForTransactions(trader) +
                spendableForOrders(trader)

        ensurePositiveAmount(spendable)
    }

    private balanceForTrades(UUID trader) {
        tradeDao.getSalesForTrader(trader).sum(0.0g) { it.total } -
                tradeDao.getBuyesForTrader(trader).sum(0.0g) { it.total }
    }

    private balanceForTransactions(UUID trader) {
        transactionDao.getByTrader(trader).sum(0.0g) { it.amount }
    }

    private spendableForTransactions(UUID trader) {
        transactionDao.getByTrader(trader).sum(0.0g) { transaction ->
            transaction.isWithdraw() || transaction.status == FINISH ? transaction.amount : 0.0g
        }
    }

    private spendableForOrders(UUID trader) {
        orderDao.getBuyOrdersForTrader(trader).sum(0.0g) { -it.total }
    }

    private BigDecimal ensurePositiveAmount(BigDecimal amount) {
        amount = amount.setScale(2, RoundingMode.HALF_UP)
        if (amount < 0g) {
            throw new FatalException('Bank balance is negative!')
        }
        amount
    }
}
