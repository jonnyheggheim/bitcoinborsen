package no.bitcoinborsen.backend.service

import no.bitcoinborsen.model.Trader

interface TraderService {
    void haveBeenAuthenticated(String username, String email)

    Trader getTraderFromOpenId(String openId)

    Iterable<Trader> listAllTraders()

    Trader getFromKidNumber(String kidNumber)
}
