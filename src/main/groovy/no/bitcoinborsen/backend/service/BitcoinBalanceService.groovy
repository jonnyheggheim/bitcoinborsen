package no.bitcoinborsen.backend.service

import java.math.RoundingMode
import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.backend.FatalException
import no.bitcoinborsen.model.TransactionStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import no.bitcoinborsen.backend.dao.*

@Bitcoin
@Service
final class BitcoinBalanceService implements BalanceService {
    @Autowired
    TradeDao tradeDao
    @Autowired
    OrderDao orderDao
    @Autowired
    BitcoinTransactionDao transactionDao

    @Override
    BigDecimal getSpendable(UUID trader) {
        assert trader != null
        BigDecimal spendable = 0.0g

        transactionDao.getByTrader(trader).each { transaction ->
            if (transaction.isWithdraw() || transaction.status == TransactionStatus.FINISH) {
                spendable += transaction.amount
            }
        }
        tradeDao.getSalesForTrader(trader).each { spendable -= it.amount }
        tradeDao.getBuyesForTrader(trader).each { spendable += it.amount }
        orderDao.getSellOrdersForTrader(trader).each { spendable -= it.amount }

        return spendable.setScale(8, RoundingMode.HALF_UP)
    }

    @Override
    BigDecimal getBalance(UUID trader) {
        assert trader != null

        BigDecimal balance = 0.0g
        transactionDao.getByTrader(trader).each { balance += it.amount}
        tradeDao.getSalesForTrader(trader).each { balance += it.amount}
        tradeDao.getBuyesForTrader(trader).each { balance -= it.amount}
        balance = balance.setScale(8, RoundingMode.HALF_UP)

        if (balance < 0) {
            throw new FatalException("Bitcoin balance for trader ${trader} is negative")
        }

        return balance
    }
}
