package no.bitcoinborsen.backend.service

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.Bitcoin
import no.bitcoinborsen.backend.dao.BitcoinTransactionDao
import no.bitcoinborsen.backend.dao.TransactionNotFoundException
import no.bitcoinborsen.model.BitcoinTransaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.paradoxs.bitcoin.client.BitcoinClient

@Slf4j
@Bitcoin
@Service
class BitcoinServiceImpl implements TransactionService<BitcoinTransaction> {
    @Bitcoin
    @Autowired
    BalanceService balanceService

    @Autowired
    BitcoinTransactionDao transactionDao
    @Autowired
    BitcoinClient bitcoinClient

    @Override
    void newWithdraw(BitcoinTransaction withdraw) {
        assert withdraw != null

        log.info("New withdrawal: ${withdraw}")
        if (withdraw.amount >= 0g) {
            throw new IllegalArgumentException("Amount is zero or positive!")
        }

        final BigDecimal amount = withdraw.amount.negate()

        BigDecimal balance = balanceService.getSpendable(withdraw.trader)
        if (balance < amount) {
            throw new NotEnoughFoundsException()
        }

        transactionDao.insert(withdraw)
    }

    @Override
    void newDeposit(BitcoinTransaction deposit) {
        assert deposit != null

        log.info("New deposit: ${deposit}")
        if (deposit.amount <= 0.0g) {
            throw new IllegalArgumentException("Amount is zero or negative!")
        }

        transactionDao.insert(deposit)
    }

    @Override
    List<BitcoinTransaction> getTransactionsForTrader(UUID trader) {
        assert trader != null
        transactionDao.getByTrader(trader)
    }

    @Override
    List<BitcoinTransaction> getQueuedWithdrawals() {
        transactionDao.getQueuedWithdrawals()
    }

    @Override
    void confirmWithdrawal(UUID id) {
        assert id != null

        BitcoinTransaction t = transactionDao.getById(id)
        String hash = sendBitcoin(t)
        transactionDao.markAsFinished(t.id, hash)
    }

    private String sendBitcoin(BitcoinTransaction transaction) {
        BigDecimal amount = transaction.amount.negate()
        log.info("Sending '${amount}' to '${transaction.address}'")
        String hash = bitcoinClient.sendToAddress(transaction.address, amount.doubleValue(), null, null)
        log.info("Sending suceeded with transaction hash: '${hash}'")

        return hash
    }

    @Override
    void appendDeposit(BitcoinTransaction transaction) {
        assert transaction != null

        String hash = transaction.getTransactionHash()
        try {
            BitcoinTransaction overriding = transactionDao.getByTransactionHash(hash)
            transactionDao.updateStatus(overriding.id, transaction.getStatus())
        } catch (TransactionNotFoundException ex) {
            newDeposit(transaction)
        }
    }
}
