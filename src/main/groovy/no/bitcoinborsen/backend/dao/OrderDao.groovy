package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.BuyOrder
import no.bitcoinborsen.model.SellOrder

interface OrderDao {
    BuyOrder getHighestBuyOrder() throws NoOrderFoundException

    SellOrder getLowestSellOrder() throws NoOrderFoundException

    void insertBuyOrder(BuyOrder buyOrder)

    void insertSellOrder(SellOrder sellOrder)

    void deleteBuyOrder(UUID orderId)

    void deleteSellOrder(UUID orderId)

    long getBuyOrderCount()

    long getSellOrderCount()

    Collection<BuyOrder> getBuyOrdersForTrader(UUID trader)

    Collection<SellOrder> getSellOrdersForTrader(UUID trader)

    Collection<BuyOrder> listBuyOrders()

    Collection<SellOrder> listSellOrders()
}
