package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.BankTransaction
import no.bitcoinborsen.model.TransactionStatus

interface BankTransactionDao {
    Collection<BankTransaction> getByTrader(UUID trader)

    long getTotalCount()

    void insert(BankTransaction transaction)

    BankTransaction getByTransactionId(String transactionId)

    void updateStatus(UUID id, TransactionStatus newStatus)

    BankTransaction getById(UUID id)
}