package no.bitcoinborsen.backend.dao

import java.sql.ResultSet
import no.bitcoinborsen.model.Trader
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository("traderDao")
final class JdbcTraderDao implements TraderDao {
    @Autowired
    JdbcOperations jdbcTemplate

    @Override
    Collection<Trader> listAllTraders() {
        String sql = "select * from trader;"
        jdbcTemplate.query(sql, new TraderRowMapper())
    }

    @Override
    Trader getTraderByOpenId(String openId) {
        String sql = 'select * from trader where openId=?;'
        try {
            return jdbcTemplate.queryForObject(sql, new TraderRowMapper(), openId)
        } catch (IncorrectResultSizeDataAccessException ex) {
            throw new TraderNotFoundException(ex)
        }
    }

    @Override
    Trader getByKidNumber(String kidNumber) {
        String sql = 'select * from trader where bank_kid=?;'
        try {
            jdbcTemplate.queryForObject(sql, new TraderRowMapper(), kidNumber)
        } catch (IncorrectResultSizeDataAccessException ex) {
            throw new TraderNotFoundException(ex)
        }
    }

    @Override
    void newTrader(Trader trader) {
        String sql = 'insert into trader (id, openid, email, bitcoin_address, bank_kid) values (?, ?, ?, ?, ?)'
        jdbcTemplate.update(sql, trader.id, trader.openId, trader.email, trader.bitcoinAddress, trader.bankKid)
    }
}

private final class TraderRowMapper implements RowMapper<Trader> {
    @Override
    Trader mapRow(ResultSet rs, int rowNum) {
        def id = UUID.fromString(rs.getString("id"))
        def openId = rs.getString("openId")
        def email = rs.getString("email")
        def address = rs.getString("bitcoin_address")
        def kid = rs.getString("bank_kid")

        new Trader(id, openId, email, address, kid)
    }
}