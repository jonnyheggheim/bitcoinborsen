package no.bitcoinborsen.backend.dao

import java.sql.ResultSet
import no.bitcoinborsen.model.BuyOrder
import no.bitcoinborsen.model.SellOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository("orderDao")
final class JdbcOrderDao implements OrderDao {
    @Autowired
    JdbcOperations jdbcTemplate

    @Override
    Collection<BuyOrder> listBuyOrders() {
        String sql = "select id, price, amount, buyer, placed from buyorder"
        jdbcTemplate.query(sql, new BuyOrderRowMapper())
    }

    @Override
    BuyOrder getHighestBuyOrder() {
        String sql = '''select id, price, amount, buyer, placed
                        from buyorder
                        order by price desc, placed asc
                        limit 1'''

        List<BuyOrder> order = jdbcTemplate.query(sql, new BuyOrderRowMapper())

        if (order.isEmpty()) {
            throw new NoOrderFoundException()
        }
        return order[0]
    }



    @Override
    SellOrder getLowestSellOrder() {
        String sql = '''select id, price, amount, seller, placed
                        from sellorder
                        order by price asc, placed asc
                        limit 1'''

        List<SellOrder> order = jdbcTemplate.query(sql, new SellOrderRowMapper())

        if (order.isEmpty()) {
            throw new NoOrderFoundException()
        }
        return order[0]
    }

    @Override
    Collection<BuyOrder> getBuyOrdersForTrader(UUID trader) {
        String sql = '''select id, price, amount, buyer, placed
                        from buyorder
                        where buyer=?
                        order by price asc, placed asc'''

        jdbcTemplate.query(sql, new BuyOrderRowMapper(), trader)
    }

    @Override
    Collection<SellOrder> listSellOrders() {
        String sql = "select id, price, amount, seller, placed from sellorder"

        jdbcTemplate.query(sql, new SellOrderRowMapper())
    }

    @Override
    Collection<SellOrder> getSellOrdersForTrader(UUID trader) {
        String sql = '''select id, price, amount, seller, placed
                        from sellorder
                        where seller=?
                        order by price asc, placed asc'''

        jdbcTemplate.query(sql, new SellOrderRowMapper(), trader)
    }

    @Override
    void insertBuyOrder(BuyOrder buyOrder) {
        String sql = '''insert into buyorder (id, price, amount, buyer, placed)
                        values(?, ?, ?, ?, ?)'''

        jdbcTemplate.update(sql, buyOrder.id,
                buyOrder.price,
                buyOrder.amount,
                buyOrder.buyer,
                buyOrder.placed)
    }

    @Override
    void insertSellOrder(SellOrder sellOrder) {
        String sql = '''insert into sellorder (id, price, amount, seller, placed)
                        values(?, ?, ?, ?, ?)'''

        jdbcTemplate.update(sql, sellOrder.id,
                sellOrder.price,
                sellOrder.amount,
                sellOrder.seller,
                sellOrder.placed)
    }

    @Override
    void deleteSellOrder(UUID orderId) {
        String sql = "delete from sellorder where id=?"
        jdbcTemplate.update(sql, orderId)
    }

    @Override
    void deleteBuyOrder(UUID orderId) {
        String sql = "delete from buyorder where id=?"
        jdbcTemplate.update(sql, orderId)
    }

    @Override
    long getSellOrderCount() {
        String sql = "select count(*) from sellorder"
        jdbcTemplate.queryForLong(sql)
    }

    @Override
    long getBuyOrderCount() {
        String sql = "select count(*) from buyorder"
        jdbcTemplate.queryForLong(sql)
    }
}

private final class BuyOrderRowMapper implements RowMapper<BuyOrder> {
    @Override
    BuyOrder mapRow(ResultSet rs, int rowNum) {
        def id = UUID.fromString(rs.getString("id"))
        def price = rs.getBigDecimal("price")
        def amount = rs.getBigDecimal("amount")
        def buyer = UUID.fromString(rs.getString("buyer"))
        def placed = rs.getTimestamp("placed")

        new BuyOrder(price, amount, buyer, placed, id)
    }
}

private final class SellOrderRowMapper implements RowMapper<SellOrder> {
    @Override
    SellOrder mapRow(ResultSet rs, int rowNum) {
        def id = UUID.fromString(rs.getString("id"))
        def price = rs.getBigDecimal("price")
        def amount = rs.getBigDecimal("amount")
        def seller = UUID.fromString(rs.getString("seller"))
        def placed = rs.getTimestamp("placed")

        new SellOrder(price, amount, seller, placed, id)
    }
}