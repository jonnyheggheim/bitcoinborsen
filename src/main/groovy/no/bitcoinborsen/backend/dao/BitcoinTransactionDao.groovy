package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.BitcoinTransaction
import no.bitcoinborsen.model.TransactionStatus

interface BitcoinTransactionDao {
    Collection<BitcoinTransaction> getByTrader(UUID trader)

    long getTotalCount()

    void insert(BitcoinTransaction transaction)

    Collection<BitcoinTransaction> getQueuedWithdrawals()

    BitcoinTransaction getById(UUID id)

    void markAsFinished(UUID id, String hash)

    BitcoinTransaction getByTransactionHash(String hash)

    void updateStatus(UUID id, TransactionStatus transactionStatus)
}
