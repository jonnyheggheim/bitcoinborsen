package no.bitcoinborsen.backend.dao


import no.bitcoinborsen.model.Trade

interface TradeDao {
    long getTradeCount()

    void insertTrade(Trade trade)

    Collection<Trade> getBuyesForTrader(UUID traderId)

    Collection<Trade> getSalesForTrader(UUID traderId)

    Collection<Trade> list(Date since)
}
