package no.bitcoinborsen.backend.dao

import groovy.transform.InheritConstructors

@InheritConstructors
class TransactionNotFoundException extends RuntimeException {
    private static final serialVersionUID = 3L
}
