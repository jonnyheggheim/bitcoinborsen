package no.bitcoinborsen.backend.dao

import java.sql.ResultSet
import no.bitcoinborsen.model.Trade
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository("tradeDao")
final class JdbcTradeDao implements TradeDao {
    @Autowired
    JdbcOperations jdbcTemplate

    @Override
    void insertTrade(Trade trade) {
        String sql = '''insert into trade (id, price, amount, buyer, seller, traded)
                        values(?, ?, ?, ?, ?, ?)'''

        jdbcTemplate.update(sql, trade.id,
                trade.price,
                trade.amount,
                trade.buyer,
                trade.seller,
                trade.traded)
    }

    @Override
    Collection<Trade> getBuyesForTrader(UUID traderId) {
        String sql = '''select id, price, amount, buyer, seller, traded
                        from trade where buyer=?'''
        jdbcTemplate.query(sql, new TradeRowMapper(), traderId)
    }

    @Override
    Collection<Trade> getSalesForTrader(UUID traderId) {
        String sql = '''select id, price, amount, buyer, seller, traded
                        from trade where seller=?'''
        jdbcTemplate.query(sql, new TradeRowMapper(), traderId)
    }

    @Override
    Collection<Trade> list(Date since) {
        String sql = '''select id, price, amount, buyer, seller, traded
                        from trade where traded>=?'''
        jdbcTemplate.query(sql, new TradeRowMapper(), since)
    }

    @Override
    long getTradeCount() {
        String sql = "select count(*) from trade"
        jdbcTemplate.queryForInt(sql)
    }
}

private final class TradeRowMapper implements RowMapper<Trade> {
    @Override
    Trade mapRow(ResultSet rs, int rowNum) {
        UUID id = UUID.fromString(rs.getString("id"))
        BigDecimal price = rs.getBigDecimal("price")
        BigDecimal amount = rs.getBigDecimal("amount")
        UUID buyer = UUID.fromString(rs.getString("buyer"))
        UUID seller = UUID.fromString(rs.getString("seller"))
        Date traded = rs.getTimestamp("traded")

        new Trade(price, amount, seller, buyer, traded, id)
    }
}