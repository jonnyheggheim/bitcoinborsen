package no.bitcoinborsen.backend

import groovy.transform.InheritConstructors

@InheritConstructors
final class FatalException extends RuntimeException {
    private static final serialVersionUID = 2L
}
