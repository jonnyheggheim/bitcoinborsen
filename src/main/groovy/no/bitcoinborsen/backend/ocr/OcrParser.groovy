package no.bitcoinborsen.backend.ocr

interface OcrParser {
    Collection<OcrTransaction> listTransactions(InputStream input)
}
