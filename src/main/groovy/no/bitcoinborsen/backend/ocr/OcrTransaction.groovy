package no.bitcoinborsen.backend.ocr

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import java.math.RoundingMode

@EqualsAndHashCode
@ToString(includeNames = true)
final class OcrTransaction implements Serializable {
    private static final serialVersionUID = 4L
    private static final ID_LENGTH = 21
    private static final ACCOUNT_NUMBER_LENGTH = 11

    final String id
    final String kid
    final BigDecimal amount
    final String accountNumber
    final Date transfered

    OcrTransaction(String id, String kid, BigDecimal amount, String accountNumber, Date transfered) {
        assert id != null && id.length() == ID_LENGTH
        assert kid != null
        assert amount != null
        assert accountNumber != null && accountNumber.length() == ACCOUNT_NUMBER_LENGTH
        assert transfered != null

        this.id = id
        this.kid = kid
        this.amount = amount.setScale(2, RoundingMode.HALF_UP)
        this.accountNumber = accountNumber
        this.transfered = transfered
    }
}
