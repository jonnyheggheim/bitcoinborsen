package no.bitcoinborsen.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import java.math.RoundingMode

@EqualsAndHashCode
@ToString(includeNames = true)
final class BitcoinTransaction implements Serializable {
    private static final serialVersionUID = 4L
    final UUID id
    final UUID trader
    final BigDecimal amount
    final String address
    final Date placed
    final String transactionHash
    final TransactionStatus status

    BitcoinTransaction(UUID id, UUID trader, BigDecimal amount, String address, Date placed,
                       String transactionHash, TransactionStatus status) {
        assert id != null
        assert trader != null
        assert amount != null && amount != 0.0g
        assert address != null
        assert placed != null
        assert transactionHash != null
        assert status != null

        this.id = id
        this.trader = trader
        this.amount = amount.setScale(8, RoundingMode.HALF_UP)
        this.address = address
        this.placed = new Date(placed.getTime())
        this.transactionHash = transactionHash
        this.status = status
    }

    Date getPlaced() {
        new Date(placed.getTime())
    }

    Boolean isDeposit() {
        amount > 0.0g
    }

    Boolean isWithdraw() {
        amount < 0.0g
    }
}
