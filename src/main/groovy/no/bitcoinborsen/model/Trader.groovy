package no.bitcoinborsen.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString(includeNames = true)
final class Trader implements Serializable {
    private static final serialVersionUID = 5L
    private static final BITCOIN_ADDRESS_LENGTH = 34
    private static final BANK_KID_LENGTH = 23
    final UUID id
    final String openId
    final String email
    final String bitcoinAddress
    final String bankKid

    Trader(UUID id, String openId, String email, String bitcoinAddress, String bankKid) {
        assert id != null
        assert openId != null
        assert email != null
        assert bitcoinAddress != null
        assert bankKid != null

        assert bitcoinAddress.length() == BITCOIN_ADDRESS_LENGTH
        assert bankKid.length() == BANK_KID_LENGTH

        this.id = id
        this.openId = openId
        this.email = email
        this.bitcoinAddress = bitcoinAddress
        this.bankKid = bankKid
    }
}
