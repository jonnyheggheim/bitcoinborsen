package no.bitcoinborsen.model

enum TransactionStatus {
    QUEUE, FINISH

    char toChar() {
        name()[0]
    }

    static TransactionStatus fromChar(String c) {
        fromChar(c.charAt(0))
    }

    static TransactionStatus fromChar(char c) {
        switch (c) {
            case 'Q': return QUEUE
            case 'F': return FINISH
        }
        throw new IllegalArgumentException("Char '${c}' is not recognized")
    }
}
