package no.bitcoinborsen.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import java.math.RoundingMode

@EqualsAndHashCode
@ToString(includeNames = true)
final class SellOrder implements Serializable {
    private static final serialVersionUID = 4L
    final UUID id
    final BigDecimal price
    final BigDecimal amount
    final UUID seller
    final Date placed

    SellOrder(BigDecimal price, BigDecimal amount, UUID seller, Date placed, UUID id) {
        assert id != null
        assert price != null && price > 0.0g
        assert amount != null && amount > 0.0g
        assert seller != null
        assert placed != null

        this.id = id
        this.price = price.setScale(2, RoundingMode.HALF_UP)
        this.amount = amount.setScale(8, RoundingMode.HALF_UP)
        this.seller = seller
        this.placed = new Date(placed.getTime())
    }

    Date getPlaced() {
        new Date(placed.getTime())
    }

    BigDecimal getTotal() {
        price * amount
    }
}
