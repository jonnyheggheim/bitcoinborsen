package ru.paradoxs.bitcoin.client

import groovy.transform.Canonical

@Canonical
class AddressInfo {
    String address = ""
    String account = ""
    double amount = 0
    long confirmations = 0
}
