package ru.paradoxs.bitcoin.client

interface BitcoinClient {

    /**
     * Copies the wallet.dat file to a backup destination
     *
     * @param destination the directory we wish to backup the wallet to
     */
    void backupWallet(String destination)

    /**
     * Returns the account associated with the given address
     *
     * @param address the address for which we want to lookup the account
     * @return the account associated with a certain address
     * @since 0.3.18
     */
    String getAccount(String address)

    /**
     * Returns a new bitcoin address for receiving payments. If account is null
     * or an empty string, then that means the default account.
     *
     * @param account the name of the account for which we want a new receiving address.
     * The address can be null or the empty string, which means the default account.
     * @return the new bitcoin address for receiving payments to this account
     * @since 0.3.18
     */
    String getAccountAddress(String account)

    /**
     * Returns the list of addresses for the given account
     *
     * @param account name of account, if null or empty it means the default account
     * @return list of addresses for the given account
     * @since 0.3.18
     */
    List<String> getAddressesByAccount(String account)

    /**
     * Returns the available balance for the default account
     *
     * @return the balance for the default account
     */
    double getBalance()

    /**
     * Returns the available balance for an account
     *
     * @param account the name of the account, or the default account if null or empty
     * @return the balance
     * @since 0.3.18
     */
    double getBalance(String account)

    /**
     * Returns the number of blocks in the longest block chain
     *
     * @return the number of blocks
     */
    int getBlockCount()

    /**
     * Returns the block number of the latest block in the longest block chain
     *
     * @return the block number
     */
    int getBlockNumber()

    /**
     * Returns the number of connections to other nodes
     *
     * @return the number of connections
     */
    int getConnectionCount()

    /**
     * Returns the proof-of-work difficulty as a multiple of the minimum difficulty
     *
     * @return the current difficulty
     */
    double getDifficulty()

    /**
     * Returns boolean true if server is trying to generate bitcoins, false otherwise
     *
     * @return true if server is trying to generate bitcoins, false otherwise
     */
    boolean getGenerate()

    /**
     * Returns the current number of hashes computed per second
     *
     * @return the number of computed hashes per second
     * @since 0.3.18
     */
    long getHashesPerSecond()

    /**
     * Returns the total amount received by addresses for account in transactions
     *
     * @param account the name of the account, or the default account if null or empty string
     * @param minimumConfirmations minimum number of confirmations for the transaction to be included in the received amount
     * @return total amount received for this account
     * @since 0.3.18
     */
    double getReceivedByAccount(String account, long minimumConfirmations)

    /**
     * Returns the total amount received by bitcoinaddress in transactions
     *
     * @param address the address we want to know the received amount for
     * @param minimumConfirmations the minimum number of confirmations for a transaction to count
     * @return total amount received
     */
    double getReceivedByAddress(String address, long minimumConfirmations)

    /**
     * Return server information, about balance, connections, blocks...etc.
     *
     * @return server information
     */
    ServerInfo getServerInfo()

    /**
     * Returns transaction information for a specific transaction ID
     *
     * @param txId the transaction ID
     * @return information about the transaction with that ID
     * @since 0.3.18
     */
    TransactionInfo getTransaction(String txId)

    /**
     * Return work information.
     *
     * @return work information
     */
    WorkInfo getWork()

    /**
     * Tries to solve the block and returns true if it was successful
     *
     * @return true if the block was solved, false otherwise
     */
    boolean getWork(String block)

    /**
     * Return help for a command
     *
     * @param command the command
     * @return the help text
     */
    String help(String command)

    /**
     * Info about all received transactions by account
     *
     * @param minimumConfirmations is the minimum number of confirmations before payments are included
     * @param includeEmpty whether to include accounts that haven't received any payments
     * @return info about the received amount by account
     * @since 0.3.18
     */
    List<AccountInfo> listReceivedByAccount(long minimumConfirmations, boolean includeEmpty)

    /**
     * Info about all received transactions by address
     *
     * @param minimumConfirmations is the minimum number of confirmations before payments are included
     * @param includeEmpty whether to include addresses that haven't received any payments
     * @return info about all received transactions by address
     */
    List<AddressInfo> listReceivedByAddress(long minimumConfirmations, boolean includeEmpty)

    /**
     * Returns a list of at most <code>count</code> number of the last transactions for an account
     *
     * @param account the account related to the transactions, the default account if null or empty
     * @param count the maximum number of transactions returned, must be > 0
     * @return a list of at most <code>count</code> number of the last transactions for an account
     * @since 0.3.18
     */
    List<TransactionInfo> listTransactions(String account, int count)

    /**
     * Returns a list of at most 10 of the last transactions for an account
     *
     * @param account the account related to the transactions
     * @return a list of at most 10 of the last transactions for an account
     * @since 0.3.18
     */
    List<TransactionInfo> listTransactions(String account)

    /**
     * Returns a list of at most 10 of the last transactions for the default account
     *
     * @return a list of at most 10 of the last transactions for the default account
     * @since 0.3.18
     */
    List<TransactionInfo> listTransactions()

    /**
     * Moves Bitcoins from one account to another on the same Bitcoin client.
     * This method will fail if there is less than amount Bitcoins with minimumConfirmations
     * confirmations in the fromAccount's balance. Returns transaction ID on success.
     *
     * @param fromAccount the account we wish to move from, the default account if null or empty string
     * @param toAccount the account we wish to move to, the default account if null or empty string
     * @param amount the amount we wish to move, rounded to the nearest 0.01
     * @param minimumConfirmations minimum number of confirmations for a transaction to count
     * @param comment a comment for this move, can be null
     * @return the transaction ID for this move of Bitcoins
     * @since 0.3.18
     */
    boolean move(String fromAccount, String toAccount, double amount, int minimumConfirmations, String comment)

    /**
     * Sends amount from account's balance to bitcoinAddress.
     * This method will fail if there is less than amount Bitcoins with minimumConfirmations
     * confirmations in the account's balance (unless account is the empty-string-named default
     * account it behaves like the #sendToAddress() method). Returns transaction ID on success.
     *
     * @param account the account we wish to send from, the default account if null or empty string
     * @param bitcoinAddress the address to which we want to send Bitcoins
     * @param amount the amount we wish to send, rounded to the nearest 0.01
     * @param minimumConfirmations minimum number of confirmations for a transaction to count
     * @param comment a comment for this transfer, can be null
     * @param commentTo a comment to this transfer, can be null
     * @return the transaction ID for this transfer of Bitcoins
     * @since 0.3.18
     */
    String sendFrom(String account, String bitcoinAddress, double amount, int minimumConfirmations, String comment, String commentTo)

    /**
     * Sends amount from the server's available balance to bitcoinAddress.
     *
     * @param bitcoinAddress the bitcoinAddress to which we want to send bitcoins
     * @param amount the amount we wish to send, rounded to the nearest 0.01
     * @param comment a comment for this transfer, can be null
     * @param commentTo a comment to this transfer, can be null
     * @return the transaction ID for this transfer of Bitcoins
     */
    String sendToAddress(String bitcoinAddress, double amount, String comment, String commentTo)

    /**
     * Sets the account associated with the given address, or removes the address if the account is null
     *
     * @param address the address to be added or removed
     * @param account if null then address is removed
     * @since 0.3.18
     */
    void setAccountForAddress(String address, String account)

    /**
     * Turn on/off coins generation
     *
     * @param isGenerate on - true, off - false
     * @param processorsCount proccesorsCount processors, -1 is unlimited
     */
    void setGenerate(boolean isGenerate, int processorsCount)

    /**
     * Stops the bitcoin server
     */
    void stop()

    /**
     * Validates a Bitcoin address
     *
     * @param address the address we want to validate
     */
    ValidatedAddressInfo validateAddress(String address)
}
