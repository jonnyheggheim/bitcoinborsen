package ru.paradoxs.bitcoin.client

import groovy.transform.Canonical

@Canonical
class TransactionInfo {
    String category     // Can be null, "generate", "send", "receive", or "move"
    double amount       // Can be positive or negative
    double fee          // Only for send, can be 0.0
    long confirmations  // only for generate/send/receive
    String txId         // only for generate/send/receive
    String otherAccount // only for move
    String message      // only for send, can be null
    String to           // only for send, can be null
}
