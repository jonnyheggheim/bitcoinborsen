package ru.paradoxs.bitcoin.client

import groovy.transform.Canonical

@Canonical
class WorkInfo {
    String midstate    // Precomputed hash state after hashing the first half of the data
    String data        // Block data
    String hash1       // Formatted hash buffer for second hash
    String target      // Little endian hash target
}
