package ru.paradoxs.bitcoin.client

import groovy.transform.Canonical

@Canonical
class ServerInfo {
    String version = ""
    double balance = 0
    long blocks = 0
    int connections = 0
    String proxy = ''
    boolean generate = false
    int usedCPUs = -1
    double difficulty = 0
    long HashesPerSecond = 0
    boolean testnet = false
}
