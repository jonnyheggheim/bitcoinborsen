package ru.paradoxs.bitcoin.client

import groovy.transform.Canonical

@Canonical
class ValidatedAddressInfo {
    Boolean isValid
    String address
    Boolean isMine
    String account
}
