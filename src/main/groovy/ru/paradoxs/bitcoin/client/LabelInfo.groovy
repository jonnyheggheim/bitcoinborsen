package ru.paradoxs.bitcoin.client

import groovy.transform.Canonical

@Canonical
class LabelInfo {
    String label = ""
    double amount = 0
    long confirmations = 0
}
