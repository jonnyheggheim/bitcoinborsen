package ru.paradoxs.bitcoin.http

interface Session {
    String sendAndReceive(String message)
}
