DROP ALL OBJECTS;

CREATE TABLE buyorder (
   id uuid NOT NULL,
   price decimal NOT NULL,
   amount decimal NOT NULL,
   buyer uuid NOT NULL,
   placed timestamp NOT NULL,
   CONSTRAINT pk_buyorder PRIMARY KEY (id)
);

CREATE TABLE sellorder (
   id uuid NOT NULL,
   price decimal NOT NULL,
   amount decimal NOT NULL,
   seller uuid NOT NULL,
   placed timestamp NOT NULL,
   CONSTRAINT pk_sellorder PRIMARY KEY (id)
);

CREATE TABLE trade (
    id uuid NOT NULL,
    price decimal NOT NULL,
    amount decimal NOT NULL,
    buyer uuid NOT NULL,
    seller uuid NOT NULL,
    traded timestamp NOT NULL,
    CONSTRAINT pk_trade PRIMARY KEY (id)
);

CREATE TABLE trader (
    id uuid NOT NULL,
    openid varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    bitcoin_address char(34) NOT NULL,
    bank_kid char(23) NOT NULL,
    CONSTRAINT pk_trader PRIMARY KEY (id)
);

CREATE TABLE bitcoin_transaction (
    id uuid NOT NULL,
    trader uuid NOT NULL,
    amount decimal NOT NULL,
    address varchar(34) NOT NULL,
    placed timestamp NOT NULL,
    transaction_hash varchar(64) NOT NULL,
    status char(1) NOT NULL, /* Q=QUEUE, F=FINISH */
    CONSTRAINT pk_bitcoin_transaction PRIMARY KEY (id)
);

CREATE TABLE bank_transaction (
    id uuid NOT NULL,
    trader uuid NOT NULL,
    amount decimal NOT NULL,
    account_number char(11) NOT NULL,
    transfered timestamp NOT NULL,
    transaction_id char(21) NOT NULL,
    status char(1) NOT NULL, /* Q=QUEUE, F=FINISH */
    CONSTRAINT pk_bank_transaction PRIMARY KEY (id)
);

alter table buyorder add foreign key (buyer) references trader(id);
alter table sellorder add foreign key (seller) references trader(id);
alter table trade add foreign key (buyer) references trader(id);
alter table trade add foreign key (seller) references trader(id);
alter table bitcoin_transaction add foreign key (trader) references trader(id);
alter table bank_transaction add foreign key (trader) references trader(id);

create unique index idx_trader_openid on trader(openid);
create unique index idx_trader_bitcoin_address on trader(bitcoin_address);
create unique index idx_trader_bank_kid on trader(bank_kid);