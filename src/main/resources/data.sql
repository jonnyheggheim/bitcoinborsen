---------------------------------------------
-- One buyer and one seller and my self ;) --
---------------------------------------------
insert into trader (id, openid, email, bitcoin_address, bank_kid) values ('739a079d-17f1-4b9a-877f-4ea89cf9d634', 'http://buyer.com', 'buyer@test.com', 'mhwnQwTbDeqAMuTcWa4k5987VuKo64FtCh', '86831350704519356920000');
insert into trader (id, openid, email, bitcoin_address, bank_kid) values ('6971af86-2ea6-44cf-90bb-f20c7940fdef', 'http://seller.com', 'seller@test.com', 'muDpUggRpa4nXKg2Dcu3NT5fLX5PHA6VA6', '80175485762152043690008');
insert into trader (id, openid, email, bitcoin_address, bank_kid) values ('1971af86-2ef6-44cf-90bb-f2fc7940ffff', 'http://hegjon.pip.verisignlabs.com/', 'hegjon@gmail.com', 'mub4Q9zidVWk1MBpkxcZTCFRYrNZYSoX7b', '80175475454230528010001');

----------------
-- Buy orders --
----------------
insert into buyorder (id, price, amount, buyer, placed)
values('f25174d0-9d46-4276-bfae-35149effaa01', 6.0, 5.0, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '2011-01-01 12:00:00');
insert into buyorder (id, price, amount, buyer, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745a02', 7.0, 5.0, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '2011-01-01 13:00:00');
insert into buyorder (id, price, amount, buyer, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745a03', 7.0, 5.0, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '2011-01-01 14:00:00');
insert into buyorder (id, price, amount, buyer, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745a04', 7.0, 5.0, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '2011-01-01 15:00:00');
insert into buyorder (id, price, amount, buyer, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745a05', 7.0, 5.0, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '2011-01-01 16:00:00');

-----------------
-- Sell orders --
-----------------
insert into sellorder (id, price, amount, seller, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745b01', 13.0, 5.0, '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 12:00:00');
insert into sellorder (id, price, amount, seller, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745b02', 12.0, 5.0, '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 13:00:00');
insert into sellorder (id, price, amount, seller, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745b03', 12.0, 5.0, '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 14:00:00');
insert into sellorder (id, price, amount, seller, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745b04', 12.0, 5.0, '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 15:00:00');
insert into sellorder (id, price, amount, seller, placed)
values('f48cdf6c-3341-4e9a-937d-339f35745b05', 12.0, 5.0, '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 16:00:00');


------------
-- Trades --
------------
insert into trade (id, price, amount, buyer, seller, traded)
values('f48cdf6c-3341-4e9a-937d-339f35745c01', 10.1, 5.1, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 10:00:00');
insert into trade (id, price, amount, buyer, seller, traded)
values('f48cdf6c-3341-4e9a-937d-339f35745c02', 10.2, 5.2, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 11:00:00');
insert into trade (id, price, amount, buyer, seller, traded)
values('f48cdf6c-3341-4e9a-937d-339f35745c03', 10.3, 5.3, '739a079d-17f1-4b9a-877f-4ea89cf9d634', '6971af86-2ea6-44cf-90bb-f20c7940fdef', '2011-01-01 12:00:00');


------------------
-- Transactions --
------------------
insert into bitcoin_transaction(id, trader, amount, address, placed, transaction_hash, status)
values('f48cdf6c-3341-4e9a-937d-339f35745d01', '6971af86-2ea6-44cf-90bb-f20c7940fdef', 40.1, 'mpydLU7aG11ha2du3tUjLvQySrMjNohb9D', '2011-01-01 08:00:00', '1e6de166720580bdf508bf0b0029fdcb6ecf2ed579d5e2cbd4e051ebc4f41a01', 'F');
insert into bitcoin_transaction(id, trader, amount, address, placed, transaction_hash, status)
values('f48cdf6c-3341-4e9a-937d-339f35745d02', '6971af86-2ea6-44cf-90bb-f20c7940fdef', 40.2, 'mpydLU7aG11ha2du3tUjLvQySrMjNohb9D', '2011-01-01 09:00:00', '1e6de166720580bdf508bf0b0029fdcb6ecf2ed579d5e2cbd4e051ebc4f41a02', 'F');
insert into bitcoin_transaction(id, trader, amount, address, placed, transaction_hash, status)
values('f48cdf6c-3341-4e9a-937d-339f35745d03', '1971af86-2ef6-44cf-90bb-f2fc7940ffff', 1000.0, 'myTyniobd7nGAPNkLwzzMooYhPmquM1386', '2011-01-01 10:00:00', '1e6de166720580bdf508bf0b0029fdcb6ecf2ed579d5e2cbd4e051ebc4f41a03', 'F');
insert into bitcoin_transaction(id, trader, amount, address, placed, transaction_hash, status)
values('f48cdf6c-3341-4e9a-937d-339f35745d04', '6971af86-2ea6-44cf-90bb-f20c7940fdef', -5.5, 'mpydLU7aG11ha2du3tUjLvQySrMjNohb9D', '2011-01-01 11:00:00', '', 'Q');

insert into bank_transaction(id, trader, amount, account_number, transfered, transaction_id, status)
values('f48cdf6c-3341-4e9a-937d-339f35745e01', '739a079d-17f1-4b9a-877f-4ea89cf9d634', 42.1, '12345678901', '2011-01-01 05:00:00', '000000000000000000001', 'F');
insert into bank_transaction(id, trader, amount, account_number, transfered, transaction_id, status)
values('f48cdf6c-3341-4e9a-937d-339f35745e02', '739a079d-17f1-4b9a-877f-4ea89cf9d634', 42.2, '12345678901', '2011-01-01 06:00:00', '000000000000000000002', 'F');
insert into bank_transaction(id, trader, amount, account_number, transfered, transaction_id, status)
values('f48cdf6c-3341-4e9a-937d-339f35745e04', '739a079d-17f1-4b9a-877f-4ea89cf9d634', -5.4, '12345678901', '2011-01-01 08:00:00', '000000000000000000003', 'Q');
insert into bank_transaction(id, trader, amount, account_number, transfered, transaction_id, status)
values('f48cdf6c-3341-4e9a-937d-339f35745e03', '1971af86-2ef6-44cf-90bb-f2fc7940ffff', 1000.0, '12345678902', '2011-01-01 07:00:00', '000000000000000000004', 'F');
