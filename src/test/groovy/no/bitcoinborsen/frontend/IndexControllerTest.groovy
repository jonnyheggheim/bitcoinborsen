package no.bitcoinborsen.frontend

import org.junit.Test

class IndexControllerTest {
    @Test
    void index() {
        def controller = new IndexController()
        assert controller.index() == 'index'
    }
}
