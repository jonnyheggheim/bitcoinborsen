package no.bitcoinborsen.frontend

import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.service.TransactionService
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.frontend.forms.WithdrawForm
import no.bitcoinborsen.model.BitcoinTransaction
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.ui.ExtendedModelMap
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class WithdrawBitcoinControllerTest {
    @Mock
    TransactionService bitcoinService
    @Mock
    DateGenerator dateGenerator
    @Mock
    IdGenerator idGenerator
    @Mock
    TraderService traderService

    @InjectMocks
    WithdrawBitcoinController controller

    @Test
    void getView() {
        def model = new ExtendedModelMap()
        assert controller.getView(model) == 'withdraw-bitcoin'
    }

    @Test
    void create() {
        def form = new WithdrawForm(address: 'xxx', amount: 666.6g)

        when(traderService.getTraderFromOpenId(eq(seller.openId))).thenReturn(seller)
        when(idGenerator.random()).thenReturn(newId)
        when(dateGenerator.now()).thenReturn(tradeDate)

        assert controller.create(form, sellerPrincipal) == 'redirect:index.html'

        verify(bitcoinService).newWithdraw(any(BitcoinTransaction))
    }
}
