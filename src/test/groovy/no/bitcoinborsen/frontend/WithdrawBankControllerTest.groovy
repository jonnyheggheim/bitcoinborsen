package no.bitcoinborsen.frontend

import no.bitcoinborsen.backend.service.BankService
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.frontend.forms.WithdrawForm
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.ui.ExtendedModelMap
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class WithdrawBankControllerTest {
    @Mock
    BankService bankService
    @Mock
    DateGenerator dateGenerator
    @Mock
    IdGenerator idGenerator
    @Mock
    TraderService traderService

    @InjectMocks
    WithdrawBankController controller

    @Test
    void getView() {
        def model = new ExtendedModelMap()
        assert controller.getView(model) == 'withdraw-bank'
    }

    @Test
    void create() {
        when(traderService.getTraderFromOpenId(eq(seller.openId))).thenReturn(seller)
        when(idGenerator.random()).thenReturn(newId)
        when(dateGenerator.now()).thenReturn(tradeDate)

        def form = new WithdrawForm(address: 'xxxxxxxxxxx', amount: 666.66g)

        assert controller.create(form, sellerPrincipal) == 'redirect:index.html'
    }
}
