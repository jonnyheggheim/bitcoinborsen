package no.bitcoinborsen.frontend

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.ui.ExtendedModelMap
import static no.bitcoinborsen.backend.Fixtures.*
import no.bitcoinborsen.backend.service.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class AccountControllerTest {
    @Mock
    TraderService traderService
    @Mock
    OrderService buyOrderService
    @Mock
    OrderService sellOrderService
    @Mock
    TradeService tradeService
    @Mock
    TransactionService bitcoinService
    @Mock
    BalanceService bitcoinBalanceService

    @InjectMocks
    AccountController controller

    @Test
    void getView() {
        def model = new ExtendedModelMap()
        when(traderService.getTraderFromOpenId(eq(seller.openId))).thenReturn(seller)

        assert controller.getView(model, sellerPrincipal) == 'account'
    }
}
