package no.bitcoinborsen.frontend

import no.bitcoinborsen.backend.service.OrderService
import no.bitcoinborsen.backend.service.TraderService
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.frontend.forms.NewOrderForm
import no.bitcoinborsen.model.BuyOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.ui.ExtendedModelMap
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class BuyOrderControllerTest {
    @Mock
    OrderService<BuyOrder> orderService
    @Mock
    DateGenerator dateGenerator
    @Mock
    IdGenerator idGenerator
    @Mock
    TraderService traderService

    @InjectMocks
    BuyOrderController controller

    @Test
    void getView() {
        def model = new ExtendedModelMap()
        assert controller.getView(model) == 'buy'
    }

    @Test
    void create() {
        def form = new NewOrderForm(amount: 5.0g, price: 6.0g)

        when(traderService.getTraderFromOpenId(eq(buyer.openId))).thenReturn(buyer)
        when(idGenerator.random()).thenReturn(newId)
        when(dateGenerator.now()).thenReturn(tradeDate)

        assert controller.create(form, buyerPrincipal) == 'redirect:/account.html'
        verify(orderService).newOrder(any(BuyOrder))
    }
}
