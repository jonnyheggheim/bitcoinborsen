package no.bitcoinborsen.frontend.admin

import no.bitcoinborsen.backend.service.TransactionService
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.ui.ExtendedModelMap
import org.springframework.web.context.request.WebRequest
import static no.bitcoinborsen.backend.Fixtures.bitcoinWithdrawal1
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class ConfirmWithdrawControllerTest {
    @Mock
    TransactionService bitcoinService
    @InjectMocks
    ConfirmWithdrawController controller

    @Test
    void index() {
        when(bitcoinService.getQueuedWithdrawals()).thenReturn([bitcoinWithdrawal1])
        def model = new ExtendedModelMap()

        assert controller.index(model) == 'admin/confirm-withdraw'
        assert model.withdrawals == [bitcoinWithdrawal1]
    }

    @Test
    void confirm() {
        def request = mock(WebRequest)
        when(request.getParameter(eq('id'))).thenReturn(bitcoinWithdrawal1.id.toString())

        assert controller.confirm(request) == 'redirect:/admin/confirm-withdraw.html'
        verify(bitcoinService).confirmWithdrawal(eq(bitcoinWithdrawal1.id))
    }
}
