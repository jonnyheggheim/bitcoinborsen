package no.bitcoinborsen.frontend.bitcoincharts

import no.bitcoinborsen.backend.service.TradeService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class TradesTest {
    @Mock
    TradeService tradeService
    @InjectMocks
    Trades trades

    private StringWriter response

    @Before
    void createResponse() {
        response = new StringWriter()
    }

    @Test
    void noTrades() {
        trades.list(response)

        assert response.toString() == '[]'
    }

    @Test
    void twoTradesUseTradesAsID() {
        when(tradeService.list(anyInt())).thenReturn([trade1, trade2])
        trades.list(response)

        String expected = '[{"date":1293872400,"price":10.10,"amount":5.10000000,"tid":1293872400000},' +
                '{"date":1293876000,"price":10.20,"amount":5.20000000,"tid":1293876000000}]'

        assert response.toString() == expected
    }
}
