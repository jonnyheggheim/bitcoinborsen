package no.bitcoinborsen.frontend.bitcoincharts

import no.bitcoinborsen.backend.service.OrderService
import no.bitcoinborsen.model.BuyOrder
import no.bitcoinborsen.model.SellOrder
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class OrderbookTest {
    @Mock
    OrderService<BuyOrder> buyOrderService
    @Mock
    OrderService<SellOrder> sellOrderService
    @InjectMocks
    Orderbook orderbook

    private StringWriter response

    @Before
    void setup() {
        orderbook.buyOrderService = buyOrderService
        orderbook.sellOrderService = sellOrderService
        response = new StringWriter()
    }

    @Test
    void noTrades() {
        orderbook.list(response)

        assert response.toString() == '{"asks":[],"bids":[]}'
    }

    @Test
    void oneBidAndNoAsks() {
        when(buyOrderService.listOrders()).thenReturn([buyOrder1])
        orderbook.list(response)

        assert response.toString() == '{"asks":[],"bids":[[6.00,5.00000000]]}'
    }

    @Test
    void oneAskAndNoBids() {
        when(sellOrderService.listOrders()).thenReturn([sellOrder1])
        orderbook.list(response)

        assert response.toString() == '{"asks":[[13.00,5.00000000]],"bids":[]}'
    }

    @Test
    void twoAskAndThreeBids() {
        when(sellOrderService.listOrders()).thenReturn([sellOrder1, sellOrder2])
        when(buyOrderService.listOrders()).thenReturn([buyOrder1, buyOrder2, buyOrder3])

        orderbook.list(response)

        def expected = '{"asks":[[13.00,5.00000000],[12.00,5.00000000]],"bids":[[6.00,5.00000000],[7.00,5.00000000],[7.00,5.00000000]]}'

        assert response.toString() == expected
    }
}
