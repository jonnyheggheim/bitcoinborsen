package no.bitcoinborsen.frontend

import no.bitcoinborsen.backend.service.TraderService
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.ui.ExtendedModelMap
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class DepositBitcoinControllerTest {
    @Mock
    TraderService traderService
    @InjectMocks
    DepositBitcoinController controller

    @Test
    void getView() {
        def model = new ExtendedModelMap()

        when(traderService.getTraderFromOpenId(eq(seller.openId))).thenReturn(seller)

        assert controller.getView(model, sellerPrincipal) == 'deposit-bitcoin'
        assert model.trader == seller
    }
}
