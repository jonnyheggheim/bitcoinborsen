package no.bitcoinborsen.model

import org.junit.Test
import static no.bitcoinborsen.model.TransactionStatus.*

class TransactionStatusTest {
    @Test
    void fromCharWhenQShouldReturnQueue() {
        assert fromChar('Q') == QUEUE
    }

    @Test
    void fromCharWhenFShouldReturnFinish() {
        assert fromChar('F') == FINISH
    }

    @Test(expected = IllegalArgumentException)
    void fromCharWhenUnknownShouldThrowException() {
        fromChar('X')
    }
}
