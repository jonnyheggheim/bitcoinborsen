package no.bitcoinborsen.model

import org.junit.Test
import static no.bitcoinborsen.backend.Fixtures.*
import static no.bitcoinborsen.model.TransactionStatus.*

class BankTransactionTest {

    @Test
    void nonFailingBehavior() {
        new BankTransaction(transactionId, seller.id, 1.2g, bankAccountNumber, transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenIdIsNull() {
        new BankTransaction(null, seller.id, 1.2g, bankAccountNumber, transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenSellerIsNull() {
        new BankTransaction(transactionId, null, 1.2g, bankAccountNumber, transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenAmountIsNull() {
        new BankTransaction(transactionId, seller.id, null, bankAccountNumber, transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenAmountIsZero() {
        new BankTransaction(transactionId, seller.id, 0.0g, bankAccountNumber, transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenAccountNumberIsNull() {
        new BankTransaction(transactionId, seller.id, 1.0g, null, transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenAccountNumberIsNotLongEnough() {
        new BankTransaction(transactionId, seller.id, 1.0g, '1234', transactionDate, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenDateIsNull() {
        new BankTransaction(transactionId, seller.id, 1.0g, bankAccountNumber, null, bankTransactionId, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenTransactionIdIsNull() {
        new BankTransaction(transactionId, seller.id, 1.0g, bankAccountNumber, transactionDate, null, QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenBankTransactionIdIsNotLongEnough() {
        new BankTransaction(transactionId, seller.id, 1.0g, bankAccountNumber, transactionDate, '123', QUEUE)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenStatusIsNull() {
        new BankTransaction(transactionId, seller.id, 1.0g, bankAccountNumber, transactionDate, bankTransactionId, null)
    }

    @Test
    void isDeposit() {
        BankTransaction transaction = bankTransaction(10.0g)

        assert transaction.isDeposit()
        assert !transaction.isWithdraw()
    }

    @Test
    void isWithdraw() {
        BankTransaction transaction = bankTransaction(-122.2g)

        assert transaction.isWithdraw()
        assert !transaction.isDeposit()
    }

    @Test
    void sameAmountButDifferentFormatShouldBeEqual() {
        BankTransaction a = new BankTransaction(transactionId, seller.id, 12.10g, bankAccountNumber, transactionDate, bankTransactionId, FINISH)
        BankTransaction b = new BankTransaction(transactionId, seller.id, new BigDecimal("12.1"), bankAccountNumber, transactionDate, bankTransactionId, FINISH)

        assert a == b
        assert a.hashCode() == b.hashCode()
        assert a.toString() == b.toString()
    }
}
