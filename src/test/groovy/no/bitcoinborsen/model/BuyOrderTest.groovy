package no.bitcoinborsen.model

import org.junit.Test
import static no.bitcoinborsen.model.Helpers.*

class BuyOrderTest {
    @Test
    void test() {
        new BuyOrder(1.0g, 1.0g, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenPriceIsNegative() {
        new BuyOrder(-1.0g, 1.0g, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenPriceIsZero() {
        new BuyOrder(0.0g, 1.0g, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenPriceIsNull() {
        new BuyOrder(null, 1.0g, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenAmountIsNegative() {
        new BuyOrder(1.0g, -0.1g, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenAmountIsZero() {
        new BuyOrder(1.0g, -0.0g, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenAmountIsNull() {
        new BuyOrder(1.0g, null, BUYER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenBuyerIsNull() {
        new BuyOrder(1.0g, 1.0g, null, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenDateIsNull() {
        new BuyOrder(1.0g, 1.0g, BUYER, null, ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenIdIsNull() {
        new BuyOrder(1.0g, 1.0g, BUYER, new Date(1), null)
    }

    @Test
    void totalShouldReturnAmountMultipliedWithPrice() {
        BuyOrder order = new BuyOrder(10.1g, 5g, BUYER, new Date(1), ID)
        assert order.total == 50.5g
    }

    @Test
    void sameAmountButDifferentFormatShouldBeEqual() {
        BuyOrder a = new BuyOrder(new BigDecimal("10.10"), 5g, BUYER, new Date(1), ID)
        BuyOrder b = new BuyOrder(10.1g, new BigDecimal("5.00"), BUYER, new Date(1), ID)

        assert a == b
        assert a.hashCode() == b.hashCode()
    }
}
