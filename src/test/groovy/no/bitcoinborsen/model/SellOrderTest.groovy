package no.bitcoinborsen.model

import org.junit.Test
import static no.bitcoinborsen.model.Helpers.*

class SellOrderTest {
    @Test
    void test() {
        new SellOrder(1.0g, 1.0g, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenPriceIsNegative() {
        new SellOrder(-1.0g, 1.0g, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenPriceIsZero() {
        new SellOrder(0.0g, 1.0g, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenPriceIsNull() {
        new SellOrder(null, 1.0g, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenAmountIsNegative() {
        new SellOrder(1.0g, -1.1g, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenAmountIsZero() {
        new SellOrder(1.0g, 0.0g, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenAmountIsNull() {
        new SellOrder(1.0g, null, SELLER, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenSellerIsNull() {
        new SellOrder(1.0g, 1.0g, null, new Date(19), ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenDateIsNull() {
        new SellOrder(1.0g, 1.0g, SELLER, null, ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenIdIsNull() {
        new SellOrder(1.0g, 1.0g, SELLER, new Date(1), null)
    }

    @Test
    void totalShouldReturnAmountMultipliedWithPrice() {
        SellOrder order = new SellOrder(10.1g, 5g, SELLER, new Date(1), ID)
        assert order.getTotal() == 50.50g
    }

    @Test
    void sameAmountButDifferentFormatShouldBeEqual() {
        SellOrder a = new SellOrder(10.10g, 5g, SELLER, new Date(1), ID)
        SellOrder b = new SellOrder(10.1g, 5.00g, SELLER, new Date(1), ID)

        assert a == b
        assert a.hashCode() == b.hashCode()
    }
}
