package no.bitcoinborsen.model

import org.junit.Test
import static no.bitcoinborsen.model.Helpers.*

class TradeTest {
    @Test
    void test() {
        new Trade(1.0g, 1.0g, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenPriceIsNegative() {
        new Trade(-1.0g, 1.0g, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenPriceIsZero() {
        new Trade(0.0g, 1.0g, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenPriceIsNull() {
        new Trade(null, 1.0g, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenAmountIsNegative() {
        new Trade(-1.0g, 1.0g, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenAmountIsZero() {
        new Trade(1.0g, 0.0g, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenAmountIsNull() {
        new Trade(1.0g, null, SELLER, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenSellerIsNull() {
        new Trade(1.0g, 1.0g, null, BUYER)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenBuyerIsNull() {
        new Trade(1.0g, 1.0g, SELLER, null)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenDateIsNull() {
        new Trade(1.0g, 1.0g, SELLER, BUYER, null, ID)
    }

    @Test(expected = AssertionError)
    void shouldThrowIllegalArgumentExceptionWhenIdIsNull() {
        new Trade(1.0g, 1.0g, SELLER, BUYER, new Date(1), null)
    }

    @Test
    void totalShouldReturnAmountMultipliedWithPrice() {
        Trade trade = new Trade(10.1g, 5g, SELLER, BUYER, new Date(1), ID)
        assert trade.getTotal() == 50.5g
    }

    @Test
    void sameAmountButDifferentFormatShouldBeEqual() {
        Trade a = new Trade(new BigDecimal("10.10"), 5.0g, SELLER, BUYER, new Date(1), ID)
        Trade b = new Trade(10.1g, new BigDecimal("5.00"), SELLER, BUYER, new Date(1), ID)

        assert a == b
        assert a.hashCode() == b.hashCode()
    }
}
