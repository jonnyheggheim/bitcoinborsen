package no.bitcoinborsen.model

import com.openpojo.random.RandomFactory
import com.openpojo.random.RandomGenerator
import com.openpojo.reflection.PojoClass
import com.openpojo.reflection.PojoField
import com.openpojo.reflection.impl.PojoClassFactory
import com.openpojo.validation.PojoValidator
import com.openpojo.validation.affirm.Affirm
import com.openpojo.validation.rule.impl.NoNestedClassRule
import com.openpojo.validation.test.Tester
import com.openpojo.validation.test.impl.GetterTester
import com.openpojo.validation.utils.ValidationHelper
import org.junit.Before
import org.junit.Test

class AllModelsTest {

    private PojoValidator pojoValidator

    @Before
    void setup() {
        RandomFactory.addRandomGenerator(new UuidRandomGenerator())

        pojoValidator = new PojoValidator()

        pojoValidator.addRule(new NoNestedClassRule())

        pojoValidator.addTester(new GetterTester())
        pojoValidator.addTester(new DateIsNotMutableTester())
        pojoValidator.addTester(new EqualsNullShouldReturnFalseTester())
        pojoValidator.addTester(new EqualsAnotherTypeShouldReturnFalseTester())
        pojoValidator.addTester(new EqualsSameInstanceShouldReturnTrueTester())
        pojoValidator.addTester(new ToStringShouldContainClassName())
    }

    private void validate(Class<?> model) {
        PojoClass pojoClass = PojoClassFactory.getPojoClass(model)
        pojoValidator.runValidation(pojoClass)
    }

    @Test
    void buyOrder() {
        validate(BuyOrder)
    }

    @Test
    void sellOrder() {
        validate(SellOrder)
    }

    @Test
    void trade() {
        validate(Trade)
    }

    @Test
    void bitcoinTransaction() {
        validate(BitcoinTransaction)
    }
}

class UuidRandomGenerator implements RandomGenerator {

    @Override
    Collection<Class<?>> getTypes() {
        [UUID]
    }

    @Override
    Object doGenerate(Class<?> type) {
        UUID.randomUUID()
    }
}

class ToStringShouldContainClassName implements Tester {

    @Override
    void run(PojoClass pojoClass) {
        Object instance = ValidationHelper.getBasicInstance(pojoClass)
        String toString = instance.toString()
        boolean result = toString.contains(pojoClass.getClazz().getSimpleName())
        Affirm.affirmTrue("toString() do not contain the class name", result)
    }
}


class DateIsNotMutableTester implements Tester {

    @Override
    void run(final PojoClass pojoClass) {
        Object instance = ValidationHelper.getBasicInstance(pojoClass)
        List<PojoField> fields = pojoClass.getPojoFields()
        for (PojoField field: fields) {
            if (field.hasGetter() && field.getType().equals(Date)) {
                validateImuteAbleDate(field, instance)
            }
        }
    }

    private void validateImuteAbleDate(PojoField field, Object instance) {
        Date isMutable = (Date) field.invokeGetter(instance)
        long original = isMutable.getTime()
        isMutable.setTime(isMutable.getTime() + 111111111L)

        Date secondTime = (Date) field.invokeGetter(instance)

        String message = String.format("Expected field not to be mutable for [%s.%s]",
                instance.getClass().getName(), field.getName())

        Affirm.affirmTrue(message, original == secondTime.getTime())
    }
}

class EqualsAnotherTypeShouldReturnFalseTester implements Tester {

    @Override
    void run(PojoClass pojoClass) {
        Object instance = ValidationHelper.getBasicInstance(pojoClass)
        boolean equals = instance.equals(new AnotherClass())
        Affirm.affirmFalse("Equals with null should return false", equals)
    }

    private static class AnotherClass {
    }
}

class EqualsNullShouldReturnFalseTester implements Tester {

    @Override
    void run(PojoClass pojoClass) {
        Object instance = ValidationHelper.getBasicInstance(pojoClass)
        boolean equals = instance.equals(null)
        Affirm.affirmFalse("Equals with null should return false", equals)
    }
}

class EqualsSameInstanceShouldReturnTrueTester implements Tester {

    @Override
    void run(PojoClass pojoClass) {
        Object instance = ValidationHelper.getBasicInstance(pojoClass)
        boolean equals = instance.equals(instance)
        Affirm.affirmTrue("Equals with same instance should return true", equals)
    }
}
