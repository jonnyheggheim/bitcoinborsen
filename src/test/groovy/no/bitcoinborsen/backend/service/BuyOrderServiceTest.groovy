package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.NoOrderFoundException
import no.bitcoinborsen.backend.dao.OrderDao
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.model.SellOrder
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import org.junit.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class BuyOrderServiceTest {
    @Mock
    OrderDao dao
    @Mock
    DateGenerator dateGenerator
    @Mock
    IdGenerator idGenerator
    @Mock
    TradeService tradeService
    @Mock
    BalanceService bankBalance
    @InjectMocks
    BuyOrderService buyOrderService

    @Before
    void configureMocks() {
        when(dateGenerator.now()).thenReturn(tradeDate)
        when(idGenerator.random()).thenReturn(tradeId)
        when(bankBalance.getSpendable(any(UUID))).thenReturn(999999.99g)

        buyOrderService.bankBalance = bankBalance
    }

    @After
    void verifyGetBalanceNeverCalled() {
        verify(bankBalance, never()).getBalance(any(UUID))
    }

    private void verifyNoOtherUpdateInteractions() {
        verify(dao, atLeast(0)).getLowestSellOrder()
        verify(dao, atLeast(0)).getHighestBuyOrder()
        verifyNoMoreInteractions(dao, tradeService)
    }

    @Test
    void whenNoMatchThenOrderShouldJustBeAddedWhenNewBuyOrder() {
        def lowestSellOrder = sellOrder(10.0g, 1.0g, new Date(1))
        def newBuyOrder = buyOrder(5.0g, 1.0g, new Date(10))

        when(dao.getLowestSellOrder()).thenReturn(lowestSellOrder)

        buyOrderService.newOrder(newBuyOrder)

        verify(dao).insertBuyOrder(eq(newBuyOrder))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void whenNoSellOrdersThenOrderShouldJustBeAddedWhenNewBuyOrder() {
        def newBuyOrder = buyOrder(5.0g, 1.0g, new Date(10))

        when(dao.getLowestSellOrder()).thenThrow(new NoOrderFoundException())

        buyOrderService.newOrder(newBuyOrder)

        verify(dao).insertBuyOrder(eq(newBuyOrder))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newBuyOrderFillHalfOfExsistingSellOrder() {
        def lowestSellOrder = sellOrder(10.0g, 10.0g, new Date(1))
        def newBuyOrder = buyOrder(11.0, 5.0g, new Date(10))

        when(dao.getLowestSellOrder()).thenReturn(lowestSellOrder)

        buyOrderService.newOrder(newBuyOrder)

        def restSellOrder = sellOrder(10.0g, 5.0g, new Date(1))
        def expectedTrade = trade(10.0g, 5.0g)
        verify(dao).deleteSellOrder(eq(lowestSellOrder.id))
        verify(dao).insertSellOrder(eq(restSellOrder))
        verify(tradeService).newTrade(eq(expectedTrade))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newBuyOrderFillExsistingSellOrderExact() {
        def lowestSellOrder = sellOrder(10.0g, 10.0g, new Date(1))
        def newBuyOrder = buyOrder(11.0g, 10.0g, new Date(10))

        when(dao.getLowestSellOrder()).thenReturn(lowestSellOrder)

        buyOrderService.newOrder(newBuyOrder)

        def expectedTrade = trade(10.0g, 10.0g)
        verify(dao).deleteSellOrder(eq(lowestSellOrder.id))
        verify(tradeService).newTrade(eq(expectedTrade))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newBuyOrderFillMoreThanExsistingSellOrder() {
        def lowestSellOrder = sellOrder(10.0g, 10.0g, new Date(1))
        def newBuyOrder = buyOrder(11.0g, 30.0g, new Date(10))

        when(dao.getLowestSellOrder()).thenReturn(lowestSellOrder).thenThrow(new NoOrderFoundException())

        buyOrderService.newOrder(newBuyOrder)

        def expectedTrade = trade(10.0g, 10.0g)
        def restBuyOrder = buyOrder(11.0g, 20.0g, new Date(10))
        verify(dao).deleteSellOrder(eq(lowestSellOrder.id))
        verify(dao).insertBuyOrder(restBuyOrder)
        verify(tradeService).newTrade(eq(expectedTrade))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newBuyOrderFillThreeAndAHalfSellOrders() {
        def lowestSellOrder1 = sellOrder(10.0g, 10.1g, new Date(1), "6e893bbb-59d2-40df-946e-f17cc03c1801")
        def lowestSellOrder2 = sellOrder(11.0g, 10.2g, new Date(2), "6e893bbb-59d2-40df-946e-f17cc03c1802")
        def lowestSellOrder3 = sellOrder(12.0g, 10.3g, new Date(3), "6e893bbb-59d2-40df-946e-f17cc03c1803")
        def lowestSellOrder4 = sellOrder(13.0g, 10.4g, new Date(4), "6e893bbb-59d2-40df-946e-f17cc03c1804")
        when(dao.getLowestSellOrder()).thenReturn(lowestSellOrder1, lowestSellOrder2, lowestSellOrder3, lowestSellOrder4)

        def newBuyOrder = buyOrder(14.0g, 36.0g, new Date(10))

        def expectedTrade1 = trade(10.0g, 10.1g)
        def expectedTrade2 = trade(11.0g, 10.2g)
        def expectedTrade3 = trade(12.0g, 10.3g)
        def expectedTrade4 = trade(13.0g, 5.4g)

        buyOrderService.newOrder(newBuyOrder)

        SellOrder restSellOrder = sellOrder(13.0g, 5.0g, new Date(4), "6e893bbb-59d2-40df-946e-f17cc03c1804")
        verify(dao).deleteSellOrder(eq(lowestSellOrder1.id))
        verify(dao).deleteSellOrder(eq(lowestSellOrder2.id))
        verify(dao).deleteSellOrder(eq(lowestSellOrder3.id))
        verify(dao).deleteSellOrder(eq(lowestSellOrder4.id))

        verify(tradeService).newTrade(eq(expectedTrade1))
        verify(tradeService).newTrade(eq(expectedTrade2))
        verify(tradeService).newTrade(eq(expectedTrade3))
        verify(tradeService).newTrade(eq(expectedTrade4))

        verify(dao).insertSellOrder(restSellOrder)
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void deleteBuyOrderShouldJustForwardToDao() {
        buyOrderService.deleteOrder(buyOrderId)
        verify(dao).deleteBuyOrder(buyOrderId)
    }
}
