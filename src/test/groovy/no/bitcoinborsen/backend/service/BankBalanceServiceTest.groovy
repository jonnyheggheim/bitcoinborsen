package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.FatalException
import no.bitcoinborsen.backend.dao.BankTransactionDao
import no.bitcoinborsen.backend.dao.OrderDao
import no.bitcoinborsen.backend.dao.TradeDao
import no.bitcoinborsen.model.Trade
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import static no.bitcoinborsen.model.TransactionStatus.FINISH
import static no.bitcoinborsen.model.TransactionStatus.QUEUE
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class BankBalanceServiceTest {
    @Mock
    TradeDao tradeDao
    @Mock
    OrderDao orderDao
    @Mock
    BankTransactionDao transactionDao
    @InjectMocks
    BankBalanceService balanceService

    @Test(expected = AssertionError)
    void getBalanceShouldThrowExceptionWhenNull() {
        balanceService.getBalance(null)
    }

    @Test(expected = AssertionError)
    void getSpendableShouldThrowExceptionWhenNull() {
        balanceService.getSpendable(null)
    }

    @Test
    void traderWithNoTradesOrTransactionsShouldHaveZeroInBalance() {
        assert balanceService.getBalance(buyer.id) == 0.0g
    }

    @Test
    void getSpendableWhenNoTradesNorTransactionShouldReturnZero() {
        assert balanceService.getSpendable(unknownTrader.id) == 0.0g
    }

    @Test(expected = FatalException)
    void ifBalanceSomehowGetsNegativeThenThrownAnException() {
        when(tradeDao.getBuyesForTrader(eq(trade3.buyer))).thenReturn([trade3])

        balanceService.getBalance(trade3.buyer)
    }

    @Test(expected = FatalException)
    void ifSpendableSomehowGetsNegativeThenThrownAnException() {
        when(tradeDao.getBuyesForTrader(eq(trade3.buyer))).thenReturn([trade3])

        balanceService.getSpendable(trade3.buyer)
    }

    @Test
    void traderWithLotsOfSalesShouldHavePlusInBalance() {
        when(tradeDao.getSalesForTrader(eq(buyer.id))).thenReturn([trade1, trade2, trade3])

        def expected = trade1.total + trade2.total + trade3.total

        assert balanceService.getBalance(buyer.id) == expected
        assert balanceService.getSpendable(buyer.id) == expected
    }

    @Test
    void traderWithTwoSalesAndOneBuyShouldHaveLessInBalance() {
        when(tradeDao.getSalesForTrader(eq(buyer.id))).thenReturn([trade1, trade2])
        when(tradeDao.getBuyesForTrader(eq(buyer.id))).thenReturn([trade3])

        def expected = trade1.total + trade2.total - trade3.total

        assert balanceService.getBalance(buyer.id) == expected
        assert balanceService.getSpendable(buyer.id) == expected
    }

    @Test
    void traderWithNoTradesButTwoBankDepositShouldHaveThatInPlus() {
        UUID trader = buyer.id
        when(transactionDao.getByTrader(eq(trader))).thenReturn([bankDeposit1, bankDeposit2])

        def expected = bankDeposit1.amount + bankDeposit2.amount

        assert balanceService.getBalance(trader) == expected
        assert balanceService.getSpendable(trader) == expected
    }

    @Test
    void openBuyOrderShouldOnlyAffectSpendable() {
        when(transactionDao.getByTrader(eq(buyer.id))).thenReturn([bankDeposit1])
        when(orderDao.getBuyOrdersForTrader(eq(buyer.id))).thenReturn([buyOrder1])

        assert balanceService.getSpendable(buyer.id) == bankDeposit1.amount - buyOrder1.total
        assert balanceService.getBalance(buyer.id) == bankDeposit1.amount
    }

    @Test
    void testWithCompleteSetOfData() {
        def trader = buyer.id
        def transactions = [
                bankTransaction(+100g, FINISH),
                bankTransaction(+200, FINISH),
                bankTransaction(-50g, FINISH), // +250

                bankTransaction(+40g, QUEUE),
                bankTransaction(+36g, QUEUE), // +0 (+76)

                bankTransaction(-10g, QUEUE),
                bankTransaction(-22g, QUEUE) // -32
        ]

        def sellTrades = [
                new Trade(2.0g, 5.0g, unknownTrader.id, trader), //  +10
                new Trade(3.0g, 10.0g, unknownTrader.id, trader) //  +30
        ]                                                        //= +40

        def buyTrades = [
                new Trade(1.0g, 70.0g, trader, unknownTrader.id), //  -70
                new Trade(2.0g, 20.0g, trader, unknownTrader.id)  //  -40
        ]                                                         //=-110

        def openSell = [sellOrder(2.0g, 7.0g, new Date(3)), sellOrder(3.0g, 5.0g, new Date(4))]     // + 0
        def openBuy = [buyOrder(1.0g, 11.0g, new Date(1)), buyOrder(3.0g, 3.0g, new Date(2))] // +0 (-20)

        def spendable = 250g + 0g - 32g + 40g - 110g + 0g - 20g
        def balance = 250g + 76g - 32g + 40g - 110g + 0g - 0g

        when(transactionDao.getByTrader(any())).thenReturn(transactions)
        when(tradeDao.getBuyesForTrader(any())).thenReturn(buyTrades)
        when(tradeDao.getSalesForTrader(any())).thenReturn(sellTrades)
        when(orderDao.getBuyOrdersForTrader(any())).thenReturn(openBuy)
        when(orderDao.getSellOrdersForTrader(any())).thenReturn(openSell)

        assert balanceService.getBalance(trader) == balance
        assert balanceService.getSpendable(trader) == spendable
    }
}
