package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.BankTransactionDao
import no.bitcoinborsen.backend.dao.TransactionNotFoundException
import no.bitcoinborsen.model.BankTransaction
import no.bitcoinborsen.model.TransactionStatus
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import org.junit.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class BankServiceImplTest {
    @Mock
    BankTransactionDao bankDao
    @Mock
    BalanceService bankBalance
    @InjectMocks
    BankServiceImpl service

    @Before
    void setupMocks() {
        when(bankBalance.getSpendable(any(UUID))).thenReturn(99999.9g)
    }

    @After
    void verifyGetBalanceNeverCalled() {
        verify(bankBalance, never()).getBalance(any(UUID))
    }

    @Test
    void newDeposit() {
        service.newDeposit(bankDeposit1)

        verify(bankDao).insert(bankDeposit1)
    }

    @Test(expected = AssertionError)
    void newDepositShouldThrowExceptionWhenTransactionIsNull() {
        service.newDeposit(null)
    }

    @Test(expected = IllegalArgumentException)
    void newDepositShouldThrowExceptionWhenAmountIsNegative() {
        try {
            service.newDeposit(bankWithdrawal1)
        } finally {
            verifyZeroInteractions(bankDao)
        }
    }

    @Test
    void newWithdraw() {
        service.newWithdraw(bankWithdrawal1)

        verify(bankDao).insert(bankWithdrawal1)
    }

    @Test(expected = AssertionError)
    void newWithdrawShouldThrowExceptionWhenTransactionIsNull() {
        service.newWithdraw(null)
    }

    @Test(expected = IllegalArgumentException)
    void newWithdrawShouldThrowExceptionWhenAmountIsPositive() {
        try {
            service.newWithdraw(bankDeposit1)
        } finally {
            verifyZeroInteractions(bankDao)
        }
    }

    @Test(expected = NotEnoughFoundsException)
    void newWithdrawShouldThrowExceptionWhenAmountIsHigherThanSpendable() {
        when(bankBalance.getSpendable(eq(bankWithdrawal1.trader))).thenReturn(0.5g)
        try {
            service.newWithdraw(bankWithdrawal1)
        } finally {
            verifyZeroInteractions(bankDao)
        }
    }

    @Test
    void appendDepositFirstTime() {
        when(bankDao.getByTransactionId(eq(bankDeposit1.transactionId))).thenThrow(TransactionNotFoundException)

        service.appendDeposit(bankDeposit1)

        verify(bankDao).insert(eq(bankDeposit1))
        verify(bankDao, never()).updateStatus(any(UUID), any(TransactionStatus))
    }

    @Test
    void appendDepositSecondTime() {
        when(bankDao.getByTransactionId(eq(bankDeposit1.transactionId))).thenReturn(bankDeposit1)

        service.appendDeposit(bankDeposit1)

        verify(bankDao, never()).insert(any(BankTransaction))
        verify(bankDao).updateStatus(eq(bankDeposit1.id), eq(bankDeposit1.status))
    }

    @Test(expected = AssertionError)
    void appendDepositShouldThrowExceptionWhenTransactionIsNull() {
        service.appendDeposit(null)
    }
}
