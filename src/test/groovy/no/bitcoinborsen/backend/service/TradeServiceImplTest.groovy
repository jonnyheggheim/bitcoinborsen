package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.TradeDao
import no.bitcoinborsen.backend.util.DateGenerator
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class TradeServiceImplTest {
    @Mock
    TradeDao dao
    @Mock
    DateGenerator dateGenerator
    @InjectMocks
    TradeServiceImpl service

    @Test
    void newTrade() {
        service.newTrade(trade1)

        verify(dao).insertTrade(same(trade1))
    }

    @Test
    void getBuysForTraderShouldJustForwardCall() {
        when(service.getBuysForTrader(eq(seller.id))).thenReturn(buyOrders)

        assert service.getBuysForTrader(seller.id) == buyOrders
    }

    @Test
    void getSalesForTraderShouldJustForwardCall() {
        when(service.getSalesForTrader(eq(seller.id))).thenReturn(sellOrders)

        assert service.getSalesForTrader(seller.id) == sellOrders
    }

    @Test
    void listDaysOfTrade() {
        Long now = 10000000L
        Long fiveDays = 5 * 24 * 60 * 60 * 1000L

        when(dateGenerator.now()).thenReturn(new Date(now))
        when(dao.list(eq(new Date(now - fiveDays)))).thenReturn([trade1])

        assert service.list(5) == [trade1]
    }

}
