package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.FatalException
import no.bitcoinborsen.backend.dao.BitcoinTransactionDao
import no.bitcoinborsen.backend.dao.OrderDao
import no.bitcoinborsen.backend.dao.TradeDao
import no.bitcoinborsen.model.Trade
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import static no.bitcoinborsen.model.TransactionStatus.FINISH
import static no.bitcoinborsen.model.TransactionStatus.QUEUE
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class BitcoinBalanceServiceTest {
    @Mock
    TradeDao tradeDao
    @Mock
    OrderDao orderDao
    @Mock
    BitcoinTransactionDao transactionDao
    @InjectMocks
    BitcoinBalanceService balanceService

    @Test(expected = AssertionError)
    void getBalanceShouldThrowExceptionWhenNull() {
        balanceService.getBalance(null)
    }

    @Test
    void traderWithNoTradesOrTransactionsShouldHaveZeroInBalance() {
        assert balanceService.getBalance(seller.id) == 0.0g
    }

    @Test
    void traderWithLotsOfSalesShouldHavePlusInBalance() {
        def sales = [trade1, trade2, trade3]

        when(tradeDao.getSalesForTrader(eq(seller.id))).thenReturn(sales)

        def actual = balanceService.getBalance(seller.id)
        def expected = trade1.amount + trade2.amount + trade3.amount

        assert actual == expected
    }

    @Test
    void traderWithTwoSalesAndOneBuyShouldHaveLessInBalance() {
        when(tradeDao.getBuyesForTrader(eq(seller.id))).thenReturn([trade3])
        when(tradeDao.getSalesForTrader(eq(seller.id))).thenReturn([trade1, trade2])

        def actual = balanceService.getBalance(seller.id)
        def expected = trade1.amount + trade2.amount - trade3.amount

        assert actual == expected
    }

    @Test
    void traderWithNoTradesButTwoBitcoinDepositOnTenEachShouldHaveThatInPlus() {
        UUID trader = seller.id
        when(transactionDao.getByTrader(eq(trader))).thenReturn([bitcoinDeposit1, bitcoinDeposit2])

        def actual = balanceService.getBalance(trader)
        def expected = bitcoinDeposit1.amount + bitcoinDeposit2.amount

        assert actual == expected
    }

    @Test(expected = FatalException)
    void ifBalanceSomehowGetsNegativeThenThrownAnException() {
        UUID trader = trade3.buyer
        when(tradeDao.getBuyesForTrader(eq(trader))).thenReturn([trade3])

        balanceService.getBalance(trader)
    }

    @Test(expected = AssertionError)
    void getSpendableShouldThrowExceptionWhenNull() {
        balanceService.getSpendable(null)
    }

    @Test
    void getSpendableWhenNoTradesNorTransactionShouldReturnZero() {
        assert balanceService.getSpendable(unknownTrader.id) == 0.0g
    }

    @Test
    void getSpendableWithLotsOfData() {
        def trader = seller.id
        def transactions = [
                bitcoinTransaction(+100g, FINISH),
                bitcoinTransaction(+200, FINISH),
                bitcoinTransaction(-50g, FINISH), // +250

                bitcoinTransaction(+40g, QUEUE),
                bitcoinTransaction(+36g, QUEUE), // +0

                bitcoinTransaction(-10g, QUEUE),
                bitcoinTransaction(-22g, QUEUE) // -32
        ]

        def buyTrades = [
                new Trade(99.9g, 20.0g, unknownTrader.id, trader),
                new Trade(98.9g, 10.0g, unknownTrader.id, trader) // +30
        ]

        def sellTrades = [
                new Trade(99.9g, 70.0g, trader, unknownTrader.id),
                new Trade(98.9g, 40.0g, trader, unknownTrader.id) // -110
        ]

        def openBuy = [buyOrder(55.3g, 11.0g, new Date(1)), buyOrder(223.0g, 12.0g, new Date(2))] // +0
        def openSell = [sellOrder(4g, 14.0g, new Date(3)), sellOrder(3g, 15.0g, new Date(4))] // - 29

        def expected = 250g + 0g - 32g + 30g - 110g + 0g - 29g

        when(transactionDao.getByTrader(any())).thenReturn(transactions)
        when(tradeDao.getBuyesForTrader(any())).thenReturn(buyTrades)
        when(tradeDao.getSalesForTrader(any())).thenReturn(sellTrades)
        when(orderDao.getBuyOrdersForTrader(any())).thenReturn(openBuy)
        when(orderDao.getSellOrdersForTrader(any())).thenReturn(openSell)

        assert balanceService.getSpendable(seller.id) == expected
    }
}
