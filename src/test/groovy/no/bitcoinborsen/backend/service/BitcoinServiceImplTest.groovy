package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.BitcoinTransactionDao
import no.bitcoinborsen.model.BitcoinTransaction
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import ru.paradoxs.bitcoin.client.BitcoinClient
import static no.bitcoinborsen.backend.Fixtures.*
import static no.bitcoinborsen.model.TransactionStatus.QUEUE
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class BitcoinServiceImplTest {
    @Mock
    BitcoinTransactionDao transactionDao
    @Mock
    BitcoinClient bitcoinClient
    @Mock
    BalanceService balanceService
    @InjectMocks
    BitcoinServiceImpl bitcoinService

    @Test
    void withdrawAllBitcoinsFromOneBuyShouldInsertNegativeBitcoinTransaction() {
        UUID trader = trade1.buyer
        when(balanceService.getSpendable(eq(trader))).thenReturn(trade1.amount)

        def amount = trade1.amount.negate()
        def withdraw = new BitcoinTransaction(transactionId, trader, amount, transactionHash, transactionDate, "", QUEUE)
        bitcoinService.newWithdraw(withdraw)

        verify(transactionDao).insert(eq(withdraw))
    }

    @Test(expected = NotEnoughFoundsException)
    void withdrawMoreBitcoinsThenAvailableShouldThrowException() {
        UUID trader = trade1.seller
        when(balanceService.getSpendable(eq(trader))).thenReturn(trade1.amount)

        def amount = -9999999.99g
        def withdraw = new BitcoinTransaction(transactionId, trader, amount, transactionHash, transactionDate, "", QUEUE)

        try {
            bitcoinService.newWithdraw(withdraw)
        } finally {
            verify(transactionDao, never()).insert(any(BitcoinTransaction))
        }
    }

    @Test(expected = IllegalArgumentException)
    void newWithdrawShouldThrowExceptionIfAmountInTransactionIsPositive() {
        bitcoinService.newWithdraw(bitcoinTransaction(5.4g))
    }

    @Test(expected = AssertionError)
    void newWithdrawShouldThrowExceptionWhenTransactionIsNull() {
        bitcoinService.newWithdraw(null)
    }

    @Test(expected = IllegalArgumentException)
    void newDepositShouldThrowExceptionIfAmountInTransactionIsNegative() {
        try {
            bitcoinService.newDeposit(bitcoinTransaction(-1.4g))
        } finally {
            verifyZeroInteractions(transactionDao)
        }
    }

    @Test
    void newDeposit() {
        def transaction = bitcoinTransaction(10.12345678g)
        bitcoinService.newDeposit(transaction)

        verify(transactionDao).insert(eq(transaction))
    }

    @Test(expected = AssertionError)
    void newDepositShouldThrowExceptionWhenTransactionIsNull() {
        bitcoinService.newDeposit(null)
    }

    @Test
    void getTransactionsForTrader() {
        def transactions = [bitcoinDeposit1]
        when(transactionDao.getByTrader(eq(seller.id))).thenReturn(transactions)

        assert bitcoinService.getTransactionsForTrader(seller.id) == transactions
    }

    @Test(expected = AssertionError)
    void getTransactionsForTraderShouldFailOnNull() {
        bitcoinService.getTransactionsForTrader(null)
    }

    @Test
    void getQueuedWithdrawals() {
        def withdrawals = [bitcoinWithdrawal1]
        when(transactionDao.getQueuedWithdrawals()).thenReturn(withdrawals)

        assert bitcoinService.getQueuedWithdrawals() == withdrawals
    }

    @Test
    void confirmWithdrawalShouldSendBitcoinsAndMarkTransactionAsFinished() {
        def hash = 'abcd'
        when(transactionDao.getById(eq(bitcoinWithdrawal1.id))).thenReturn(bitcoinWithdrawal1)
        when(bitcoinClient.sendToAddress(anyString(), anyDouble(), anyString(), anyString())).thenReturn(hash)

        bitcoinService.confirmWithdrawal(bitcoinWithdrawal1.id)

        def amount = bitcoinWithdrawal1.amount.negate()

        verify(bitcoinClient).sendToAddress(eq(bitcoinWithdrawal1.address), eq(amount.toDouble()), any(), any())
        verify(transactionDao).markAsFinished(eq(bitcoinWithdrawal1.id), eq(hash))
    }

    @Test(expected = AssertionError)
    void confirmWithdrawalShouldThrowExceptionWhenIdIsNull() {
        bitcoinService.confirmWithdrawal(null)
    }

    @Test(expected = AssertionError)
    void appendDepositShouldThrowExceptionWhenTransactionIsNull() {
        bitcoinService.appendDeposit(null)
    }
}
