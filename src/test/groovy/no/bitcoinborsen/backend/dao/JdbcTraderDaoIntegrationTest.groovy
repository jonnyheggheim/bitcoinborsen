package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.Trader
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import static no.bitcoinborsen.backend.Fixtures.*

class JdbcTraderDaoIntegrationTest extends JdbcIntegrationTestBase {
    @Autowired
    JdbcTraderDao traderDao

    @Test
    void getTraderByOpenId() {
        Trader actual = traderDao.getTraderByOpenId(seller.openId)
        assert actual == seller
    }

    @Test(expected = TraderNotFoundException)
    void getTraderByOpenIdShouldThrowExceptionWhenNoTraderNoFound() {
        traderDao.getTraderByOpenId("http://xxx.com")
    }

    @Test
    void getByKidNumber() {
        assert traderDao.getByKidNumber(seller.bankKid) == seller
    }

    @Test(expected = TraderNotFoundException)
    void getByKidNumberShouldThrowExceptionWhenNoTraderFound() {
        traderDao.getByKidNumber("0000000000000000000000")
    }

    @Test
    void insertTrader() {
        traderDao.newTrader(unknownTrader)
        def traders = traderDao.listAllTraders()

        assert traders.size() == 4
        assert unknownTrader in traders
    }

    @Test
    void listAllTraders() {
        def traders = traderDao.listAllTraders()

        assert traders.size() == 3
        assert buyer in traders
        assert seller in traders
        assert jonny in traders
    }
}
