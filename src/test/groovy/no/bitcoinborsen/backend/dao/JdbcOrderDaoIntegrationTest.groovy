package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.BuyOrder
import no.bitcoinborsen.model.SellOrder
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import static no.bitcoinborsen.backend.Fixtures.*

class JdbcOrderDaoIntegrationTest extends JdbcIntegrationTestBase {
    @Autowired
    JdbcOrderDao orderDao

    @Test
    void getLowestSellOrder() {
        assert orderDao.getLowestSellOrder() == sellOrder2
    }

    @Test
    void getHighestBuyOrder() {
        assert orderDao.getHighestBuyOrder() == buyOrder2
    }

    @Test
    void insertBuyOrder() {
        def count = orderDao.getBuyOrderCount()
        UUID orderId = UUID.fromString("fad27a5d-b6ac-4930-85ce-0ad8e9ff4ec8")
        def order = new BuyOrder(2.0g, 2.0g, buyer.id, new Date(10), orderId)
        orderDao.insertBuyOrder(order)

        assert orderDao.getBuyOrderCount() == count + 1
    }

    @Test(expected = Exception)
    void insertBuyOrderWithInvalidBuyerShouldThrowException() {
        UUID orderId = UUID.fromString("b438cc97-cde4-4020-a89d-83e460197e43")
        def order = new BuyOrder(1.0g, 1.0g, unknownTrader.id, new Date(1), orderId)

        orderDao.insertBuyOrder(order)
    }

    @Test
    void listBuyOrders() {
        def orders = orderDao.listBuyOrders()

        assert orders.size() == 5
        assert orders.containsAll(buyOrders)
    }

    @Test
    void getBuyOrdersForTraderWithLotsOfBuyOrders() {
        def orders = orderDao.getBuyOrdersForTrader(buyer.id)

        assert orders.size() == 5
        assert orders.containsAll(buyOrders)
    }

    @Test
    void getBuyOrdersForTraderWithNoBuyOrders() {
        def orders = orderDao.getBuyOrdersForTrader(seller.id)
        assert orders.isEmpty()
    }

    @Test
    void insertSellOrder() {
        def count = orderDao.getSellOrderCount()

        def order = sellOrder(100.0g, 100.0g, new Date(1000))
        orderDao.insertSellOrder(order)

        assert orderDao.getSellOrderCount() == count + 1
    }

    @Test(expected = Exception)
    void insertSellOrderWithInvalidSellerShouldThrowException() {
        UUID orderId = UUID.fromString("b438cc97-cde4-4020-a89d-83e460197e43")

        def order = new SellOrder(100.0g, 100.0g, unknownTrader.id, new Date(1000), orderId)
        orderDao.insertSellOrder(order)
    }

    @Test
    void deleteBuyOrder() {
        def count = orderDao.getBuyOrderCount()

        orderDao.deleteBuyOrder(buyOrder3.id)
        assert orderDao.getBuyOrderCount() == count - 1
    }

    @Test
    void deleteSellOrder() {
        long count = orderDao.getSellOrderCount()

        orderDao.deleteSellOrder(sellOrder3.id)
        assert orderDao.getSellOrderCount() == count - 1
    }

    @Test
    void listSellOrders() {
        def orders = orderDao.listSellOrders()
        assert orders.size() == 5
        assert orders.containsAll(sellOrders)
    }

    @Test
    void getSellOrdersForTraderWithLotsOfSellOrders() {
        def orders = orderDao.getSellOrdersForTrader(seller.id)

        assert orders.size() == 5
        assert orders.containsAll(sellOrders)
    }

    @Test
    void getSellOrdersForTraderWithNoSellOrders() {
        def orders = orderDao.getSellOrdersForTrader(buyer.id)
        assert orders.isEmpty()
    }
}
