package no.bitcoinborsen.backend.dao

import org.junit.runner.RunWith
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import org.springframework.context.annotation.*

@RunWith(SpringJUnit4ClassRunner)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = [TestContextConfiguration.class])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
abstract class JdbcIntegrationTestBase {
}

@Configuration
@ImportResource(['classpath:/spring-jdbc.xml'])
class TestContextConfiguration {
    @Bean
    JdbcOrderDao orderDao() {
        new JdbcOrderDao()
    }

    @Bean
    JdbcTradeDao tradeDao() {
        new JdbcTradeDao()
    }

    @Bean
    JdbcBitcoinTransactionDao bitcoinTransactionDao() {
        new JdbcBitcoinTransactionDao()
    }

    @Bean
    JdbcBankTransactionDao bankTransactionDao() {
        new JdbcBankTransactionDao()
    }

    @Bean
    JdbcTraderDao traderDao() {
        new JdbcTraderDao()
    }
}