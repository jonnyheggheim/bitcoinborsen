package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.BitcoinTransaction
import no.bitcoinborsen.model.TransactionStatus
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import static no.bitcoinborsen.backend.Fixtures.*

class JdbcBitcoinTransactionDaoIntegrationTest extends JdbcIntegrationTestBase {
    @Autowired
    JdbcBitcoinTransactionDao dao

    @Test
    void getById() {
        def actual = dao.getById(bitcoinDeposit1.id)
        assert actual == bitcoinDeposit1
    }

    @Test(expected = TransactionNotFoundException)
    void getByIdShouldThrowExceptionIfNotFound() {
        UUID id = UUID.fromString("4b4fa5f9-be66-408e-a124-000000000000")
        dao.getById(id)
    }

    @Test
    void getByTransactionHash() {
        def actual = dao.getByTransactionHash(bitcoinDeposit1.transactionHash)
        assert actual == bitcoinDeposit1
    }

    @Test(expected = TransactionNotFoundException)
    void getByTransactionHashShouldThrowExceptionIfNotFound() {
        def hash = '1e6de166720580bdf508bf0b0029fdcb6e000000000000000000000000000000'
        dao.getByTransactionHash(hash)
    }

    @Test
    void getBitcoinTransactionsOnTraderWithoutTransactionShouldReturnEmptyCollection() {
        UUID trader = UUID.fromString('4b4fa5f9-be66-408e-a124-000000000000')
        def transactions = dao.getByTrader(trader)
        assert transactions.isEmpty()
    }

    @Test
    void getBitcoinTransactionsOnTraderWithTransactionShouldReturnEverything() {
        def transactions = dao.getByTrader(seller.id)

        assert transactions.size() == 3
        assert bitcoinDeposit1 in transactions
        assert bitcoinDeposit2 in transactions
        assert bitcoinWithdrawal1 in transactions
    }

    @Test
    void getQueuedWithdrawals() {
        def withdrawals = dao.getQueuedWithdrawals()
        assert withdrawals == [bitcoinWithdrawal1]
    }

    @Test
    void markAsFinished() {
        dao.markAsFinished(bitcoinWithdrawal1.id, "653177cda139d571abf065dc69bc8d698ef803189836cf5277b24656d17f34df")
        def withdrawals = dao.getQueuedWithdrawals()
        assert withdrawals.isEmpty()
    }

    @Test
    void insertBitcoinTransaction() {
        def count = dao.getTotalCount()
        def transaction = new BitcoinTransaction(transactionId, seller.id, 1.0g, bitcoinAddress, transactionDate, "", TransactionStatus.FINISH)
        dao.insert(transaction)

        assert dao.getTotalCount() == count + 1
    }

    @Test(expected = Exception)
    void insertBitcoinTransactionWithUnknownTraderShouldThrowException() {
        def transaction = new BitcoinTransaction(transactionId, unknownTrader.id, 1.0g, bitcoinAddress, transactionDate, "", TransactionStatus.FINISH)
        dao.insert(transaction)
    }

    @Test
    void updateStatus() {
        assert bitcoinDeposit1.status == TransactionStatus.FINISH

        dao.updateStatus(bitcoinDeposit1.id, TransactionStatus.QUEUE)

        def updated = dao.getById(bitcoinDeposit1.id)
        assert updated.status == TransactionStatus.QUEUE
    }
}
