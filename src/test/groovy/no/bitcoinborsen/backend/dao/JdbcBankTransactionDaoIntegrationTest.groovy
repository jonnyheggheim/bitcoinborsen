package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.BankTransaction
import no.bitcoinborsen.model.TransactionStatus
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import static no.bitcoinborsen.backend.Fixtures.*

class JdbcBankTransactionDaoIntegrationTest extends JdbcIntegrationTestBase {
    @Autowired
    JdbcBankTransactionDao dao

    @Test
    void getBankTransactionsOnTraderWithoutTransactionShouldReturnEmptyCollection() {
        def transactions = dao.getByTrader(unknownTrader.id)

        assert transactions.isEmpty()
    }

    @Test
    void getBankTransactionsOnTraderWithTransactionShouldReturnEverything() {
        def transactions = dao.getByTrader(buyer.id)

        assert transactions.size() == 3
        assert bankDeposit1 in transactions
        assert bankDeposit2 in transactions
        assert bankWithdrawal1 in transactions
    }

    @Test
    void insertBankTransaction() {
        def count = dao.getTotalCount()
        def transaction = new BankTransaction(newId, buyer.id, 1.0g, bankAccountNumber, transactionDate, bankTransactionId, TransactionStatus.FINISH)
        dao.insert(transaction)

        assert dao.getTotalCount() == count + 1
    }

    @Test(expected = Exception)
    void insertBankTransactionWithUnknownTraderShouldThrowException() {
        def transaction = new BankTransaction(unknownTrader.id, unknownTrader.id, 1.0g, bankAccountNumber, transactionDate, bankTransactionId, TransactionStatus.FINISH)
        dao.insert(transaction)
    }

    @Test
    void getByTransactionId() {
        assert dao.getByTransactionId(bankDeposit1.transactionId) == bankDeposit1
    }

    @Test(expected = TransactionNotFoundException)
    void getByTransactionIdWhenNotFoundShouldThrowException() {
        dao.getByTransactionId('123456789012345678901')
    }

    @Test
    void updateStatus() {
        assert bankDeposit1.status == TransactionStatus.FINISH

        dao.updateStatus(bankDeposit1.id, TransactionStatus.QUEUE)

        def updated = dao.getById(bankDeposit1.id)
        assert updated.status == TransactionStatus.QUEUE
    }

    @Test
    void getById() {
        assert dao.getById(bankWithdrawal1.id) == bankWithdrawal1
    }

    @Test(expected = TransactionNotFoundException)
    void getByIdShouldThrowExceptionIfTransactionIsNotFound() {
        dao.getById(unknownId)
    }
}
