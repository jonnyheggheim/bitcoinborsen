package no.bitcoinborsen.backend.ocr

import org.junit.Test

class OcrTransactionTest {
    @Test
    void ensureAmountIsSame() {
        String id = "123456789012345678901"
        String kid = "000"

        def a = new OcrTransaction(id, kid, 1.000g, '12345678910', new Date(5))
        def b = new OcrTransaction(id, kid, new BigDecimal("1.0"), '12345678910', new Date(5))
        def c = new OcrTransaction(id, kid, 1g, '12345678910', new Date(5))

        assert a == b
        assert a == c
        assert b == c
    }
}
