package no.bitcoinborsen.backend.util

import org.junit.Test

class DefaultIdGeneratorIntegrationTest {

    @Test
    void testRandom() {
        UUID anotherRandom = UUID.randomUUID()
        DefaultIdGenerator idGenerator = new DefaultIdGenerator()
        UUID actual = idGenerator.random()

        assert actual != anotherRandom
    }
}
