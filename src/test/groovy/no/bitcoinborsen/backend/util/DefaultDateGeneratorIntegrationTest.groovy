package no.bitcoinborsen.backend.util

import org.junit.Test

class DefaultDateGeneratorIntegrationTest {

    @Test
    void testNow() {
        Date now = new Date()
        DefaultDateGenerator dateGenerator = new DefaultDateGenerator()
        Date actual = dateGenerator.now()

        assert actual >= now
    }
}
