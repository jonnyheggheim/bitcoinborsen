package no.bitcoinborsen.backend

import java.security.Principal
import no.bitcoinborsen.model.*
import static no.bitcoinborsen.model.TransactionStatus.*

class Fixtures {
    //Traders
    static buyer = new Trader(uuid('739a079d-17f1-4b9a-877f-4ea89cf9d634'), 'http://buyer.com', 'buyer@test.com', 'mhwnQwTbDeqAMuTcWa4k5987VuKo64FtCh', '86831350704519356920000')
    static seller = new Trader(uuid('6971af86-2ea6-44cf-90bb-f20c7940fdef'), 'http://seller.com', 'seller@test.com', 'muDpUggRpa4nXKg2Dcu3NT5fLX5PHA6VA6', '80175485762152043690008')
    static jonny = new Trader(uuid('1971af86-2ef6-44cf-90bb-f2fc7940ffff'), 'http://hegjon.pip.verisignlabs.com/', 'hegjon@gmail.com', 'mub4Q9zidVWk1MBpkxcZTCFRYrNZYSoX7b', '80175475454230528010001')
    static unknownTrader = new Trader(uuid('ee60d4d4-bdb1-45c3-82c7-27f56ab24282'), 'http://unknown.com', 'unknown@test.com', 'msiXgMypixNwDo5btvoy4Ni74MvM9u3m1L', '90231993934029612780005')

    //Buy orders
    static buyOrder1 = new BuyOrder(6.0g, 5.0g, buyer.id, date(2011, 01, 01, 12, 00), uuid('f25174d0-9d46-4276-bfae-35149effaa01'))
    static buyOrder2 = new BuyOrder(7.0g, 5.0g, buyer.id, date(2011, 01, 01, 13, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745a02'))
    static buyOrder3 = new BuyOrder(7.0g, 5.0g, buyer.id, date(2011, 01, 01, 14, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745a03'))
    static buyOrder4 = new BuyOrder(7.0g, 5.0g, buyer.id, date(2011, 01, 01, 15, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745a04'))
    static buyOrder5 = new BuyOrder(7.0g, 5.0g, buyer.id, date(2011, 01, 01, 16, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745a05'))
    static buyOrders = [buyOrder1, buyOrder2, buyOrder3, buyOrder4, buyOrder5]

    //Sell orders
    static sellOrder1 = new SellOrder(13.0g, 5.0g, seller.id, date(2011, 01, 01, 12, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745b01'))
    static sellOrder2 = new SellOrder(12.0g, 5.0g, seller.id, date(2011, 01, 01, 13, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745b02'))
    static sellOrder3 = new SellOrder(12.0g, 5.0g, seller.id, date(2011, 01, 01, 14, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745b03'))
    static sellOrder4 = new SellOrder(12.0g, 5.0g, seller.id, date(2011, 01, 01, 15, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745b04'))
    static sellOrder5 = new SellOrder(12.0g, 5.0g, seller.id, date(2011, 01, 01, 16, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745b05'))
    static sellOrders = [sellOrder1, sellOrder2, sellOrder3, sellOrder4, sellOrder5]

    //Trades
    static trade1 = new Trade(10.1g, 5.1g, seller.id, buyer.id, date(2011, 01, 01, 10, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745c01'))
    static trade2 = new Trade(10.2g, 5.2g, seller.id, buyer.id, date(2011, 01, 01, 11, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745c02'))
    static trade3 = new Trade(10.3g, 5.3g, seller.id, buyer.id, date(2011, 01, 01, 12, 00), uuid('f48cdf6c-3341-4e9a-937d-339f35745c03'))

    //Transactoins
    static bitcoinDeposit1 = new BitcoinTransaction(uuid('f48cdf6c-3341-4e9a-937d-339f35745d01'), seller.id, 40.1g, 'mpydLU7aG11ha2du3tUjLvQySrMjNohb9D', date(2011, 01, 01, 8, 00), '1e6de166720580bdf508bf0b0029fdcb6ecf2ed579d5e2cbd4e051ebc4f41a01', FINISH)
    static bitcoinDeposit2 = new BitcoinTransaction(uuid('f48cdf6c-3341-4e9a-937d-339f35745d02'), seller.id, 40.2g, 'mpydLU7aG11ha2du3tUjLvQySrMjNohb9D', date(2011, 01, 01, 9, 00), '1e6de166720580bdf508bf0b0029fdcb6ecf2ed579d5e2cbd4e051ebc4f41a02', FINISH)
    static bitcoinWithdrawal1 = new BitcoinTransaction(uuid('f48cdf6c-3341-4e9a-937d-339f35745d04'), seller.id, -5.5g, 'mpydLU7aG11ha2du3tUjLvQySrMjNohb9D', date(2011, 01, 01, 11, 00), '', QUEUE)
    static bankDeposit1 = new BankTransaction(uuid('f48cdf6c-3341-4e9a-937d-339f35745e01'), buyer.id, 42.1g, '12345678901', date(2011, 01, 01, 5, 00), '000000000000000000001', FINISH)
    static bankDeposit2 = new BankTransaction(uuid('f48cdf6c-3341-4e9a-937d-339f35745e02'), buyer.id, 42.2g, '12345678901', date(2011, 01, 01, 6, 00), '000000000000000000002', FINISH)
    static bankWithdrawal1 = new BankTransaction(uuid('f48cdf6c-3341-4e9a-937d-339f35745e04'), buyer.id, -5.4g, '12345678901', date(2011, 01, 01, 8, 00), '000000000000000000003', QUEUE)

    //Default values for new objects
    static buyOrderId = uuid('4555ad1f-6716-4eed-8ced-8a01b463358b')
    static sellOrderId = uuid('187c09b8-21ce-4034-abc1-0ac8c9d97a87')
    static tradeId = uuid('58aa7484-f433-45e4-815d-d8654d271ba7')
    static transactionId = uuid('58aa7484-f433-45e4-815d-d8654d271777')
    static bitcoinAddress = 'mn5ZDSETdaM5mt8YnshHhVVJzH9ts23Ums'
    static bankAccountNumber = '12345678901'
    static transactionHash = 'dbcd31b22d4d06bb9a96fb90dc4200e16fb9bc066d2db536199bf12133eb761c'
    static bankTransactionId = '123456789012345678901'
    static tradeDate = new Date(100)
    static transactionDate = new Date(101)
    static unknownId = uuid('66666666-6666-6666-6666-666666666666')
    static newId = uuid('88888888-8888-8888-8888-888888888888')

    static sellerPrincipal = [getName: {seller.openId}] as Principal
    static buyerPrincipal = [getName: {buyer.openId}] as Principal

    static Date date(int year, int month, int day, int hour, int min) {
        Calendar cal = new GregorianCalendar(year, month - 1, day, hour, min)
        cal.getTime()
    }

    static UUID uuid(String uuid) {
        UUID.fromString(uuid)
    }

    static BuyOrder buyOrder(BigDecimal price, BigDecimal amount, Date placed) {
        new BuyOrder(price, amount, buyer.id, placed, buyOrderId)
    }

    static SellOrder sellOrder(BigDecimal price, BigDecimal amount, Date placed) {
        new SellOrder(price, amount, seller.id, placed, sellOrderId)
    }

    static SellOrder sellOrder(BigDecimal price, BigDecimal amount, Date placed, String orderId) {
        new SellOrder(price, amount, seller.id, placed, uuid(orderId))
    }

    static Trade trade(BigDecimal price, BigDecimal amount) {
        new Trade(price, amount, seller.id, buyer.id, tradeDate, tradeId)
    }

    static BitcoinTransaction bitcoinTransaction(BigDecimal amount) {
        new BitcoinTransaction(transactionId, seller.id, amount, bitcoinAddress, transactionDate, transactionHash, QUEUE)
    }

    static BitcoinTransaction bitcoinTransaction(BigDecimal amount, TransactionStatus status) {
        new BitcoinTransaction(transactionId, seller.id, amount, bitcoinAddress, transactionDate, transactionHash, status)
    }

    static BankTransaction bankTransaction(BigDecimal amount) {
        new BankTransaction(transactionId, buyer.id, amount, bankAccountNumber, transactionDate, bankTransactionId, QUEUE)
    }

    static BankTransaction bankTransaction(BigDecimal amount, TransactionStatus status) {
        new BankTransaction(transactionId, buyer.id, amount, bankAccountNumber, transactionDate, bankTransactionId, status)
    }
}
