package no.bitcoinborsen.security

import no.bitcoinborsen.backend.service.TraderService
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class AuthenticateAllUsersTest {
    @Mock
    TraderService traderService
    @InjectMocks
    AuthenticateAllUsers authenticate

    @After
    void verifyHaveBeenAuthenticated() {
        verify(traderService).haveBeenAuthenticated(anyString(), anyString())
    }

    @Test
    void normalUser() {
        authenticate.loadUserByUsername('xxx').with {
            assert username == 'xxx'
            assert authorities*.authority == ['ROLE_USER']
        }
    }

    @Test
    void admin() {
        authenticate.admin = 'admin'
        authenticate.loadUserByUsername('admin').with {
            assert username == 'admin'
            assert authorities*.authority == ['ROLE_ADMIN', 'ROLE_USER']
        }
    }
}
