package no.bitcoinborsen.security

import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.security.authentication.TestingAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import static org.mockito.Mockito.verify

@RunWith(MockitoJUnitRunner)
class AddUserToMdcFilterTest {
    @Mock
    FilterChain chain
    @Mock
    HttpServletRequest request
    @Mock
    HttpServletResponse response
    @InjectMocks
    AddUserToMdcFilter filter

    @After
    void verifyDoFilter() {
        verify(chain).doFilter(request, response)
    }

    @Test
    void loggedInUser() {
        def user = [getUsername: { 'mock' }] as UserDetails
        SecurityContextHolder.context.authentication = new TestingAuthenticationToken(user, null)
        filter.doFilter(request, response, chain)
    }

    @Test
    void anonymous() {
        SecurityContextHolder.context.authentication = new TestingAuthenticationToken('anonymous', null)
        filter.doFilter(request, response, chain)
    }
}
