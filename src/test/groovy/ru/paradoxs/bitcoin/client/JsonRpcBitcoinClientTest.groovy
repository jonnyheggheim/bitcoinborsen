package ru.paradoxs.bitcoin.client

import no.bitcoinborsen.backend.util.IdGenerator
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.invocation.InvocationOnMock
import org.mockito.runners.MockitoJUnitRunner
import org.mockito.stubbing.Answer
import ru.paradoxs.bitcoin.http.Session
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class JsonRpcBitcoinClientTest {
    @Mock
    Session session
    @Mock
    IdGenerator idGenerator
    @InjectMocks
    JsonRpcBitcoinClient client

    private expect(method, params, result = '""') {
        def uuid = 'ac55d4cb-16a3-4a2e-adce-6e7192c658c7'
        final request = """{"jsonrpc":"2.0","id":"${uuid}","method":"${method}","params":${params}}"""
        when(session.sendAndReceive(anyString())).thenAnswer(new Answer<String>() {
            String answer(InvocationOnMock invocation) {
                assert invocation.arguments[0] == request
                "{\"result\": ${result}}"
            }
        })
        when(idGenerator.random()).thenReturn(UUID.fromString(uuid))
    }

    @Test
    void getAccount() {
        expect('getaccount', '["mock"]', '"xxx"')
        assert client.getAccount('mock') == 'xxx'
    }

    @Test
    void getBalance() {
        expect('getbalance', '[]', '1234.12345678')
        assert client.getBalance() == 1234.12345678
    }

    @Test
    void getBalanceWithParameter() {
        expect('getbalance', '["test-account"]', '12345.12345678')
        assert client.getBalance('test-account') == 12345.12345678
    }

    @Test
    void getBalanceWithNullParameter() {
        expect('getbalance', '[""]', '12.0')
        assert client.getBalance(null) == 12.0
    }

    @Test
    void getBlockCount() {
        expect('getblockcount', '[]', '152785')
        assert client.getBlockCount() == 152785
    }

    @Test
    void getBlockNumber() {
        expect('getblocknumber', '[]', '152786')
        assert client.getBlockNumber() == 152786
    }

    @Test
    void getConnectionCount() {
        expect('getconnectioncount', '[]', '123')
        assert client.getConnectionCount() == 123
    }

    @Test
    void getAddressesByAccount() {
        expect('getaddressesbyaccount', '["mock"]', '["yyy", "xxx"]')
        assert client.getAddressesByAccount('mock') == ['yyy', 'xxx']
    }

    @Test
    void getHashesPerSecond() {
        expect('gethashespersec', '[]', '4567')
        assert client.getHashesPerSecond() == 4567
    }

    @Test
    void getDifficulty() {
        expect('getdifficulty', '[]', '1192497.75008948')
        assert client.getDifficulty() == 1192497.75008948
    }

    @Test
    void getGenerate() {
        expect('getgenerate', '[]', 'true')
        assert client.getGenerate() == true
    }

    @Test
    void setGenerate() {
        expect('setgenerate', '[true,3]')
        client.setGenerate(true, 3)
    }

    @Test
    void getAccountAddress() {
        expect('getaccountaddress', '["test"]', '"1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd"')
        assert client.getAccountAddress('test') == '1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd'
    }

    @Test
    void getAccountAddressWithNullParameter() {
        expect('getaccountaddress', '[""]', '"1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd"')
        assert client.getAccountAddress(null) == '1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd'
    }

    @Test
    void setAccountForAddress() {
        expect('setaccount', '["1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd","test"]')
        client.setAccountForAddress('1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd', 'test')
    }

    @Test
    void getReceivedByAddress() {
        expect('getreceivedbyaddress', '["1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd",3]', '1.12345678')
        assert client.getReceivedByAddress('1PWoV47jaPy3xJx7KuStoQ9upLx2ETjwcd', 3) == 1.12345678
    }

    @Test
    void getReceivedByAccount() {
        expect('getreceivedbyaccount', '["xxx",4]', '123456.12345678')
        assert client.getReceivedByAccount('xxx', 4) == 123456.12345678
    }

    @Test
    void getWorkWithoutParameter() {
        def response = '''\
{
    "midstate" : "f7fcc1da2a9be971541595955c624fa833503af2f46c873850156969d1044e1d",
    "data" : "00000001c4019dc0c54b83adc41759a64d1dba09174ffd33bce8dfec0000024b00000000c3599055321d92e4321180ee1c3ebac12a0b787d5cd16a72f6ba7d45ea9f169f4ec638301a0e119a00000000000000800000000000000000000000000000000000000000000000000000000000000000000000000000000080020000",
    "hash1" : "00000000000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000010000",
    "target" : "00000000000000000000000000000000000000000000009a110e000000000000"
}'''

        def work = new WorkInfo(
                midstate: 'f7fcc1da2a9be971541595955c624fa833503af2f46c873850156969d1044e1d',
                data: '00000001c4019dc0c54b83adc41759a64d1dba09174ffd33bce8dfec0000024b00000000c3599055321d92e4321180ee1c3ebac12a0b787d5cd16a72f6ba7d45ea9f169f4ec638301a0e119a00000000000000800000000000000000000000000000000000000000000000000000000000000000000000000000000080020000',
                hash1: '00000000000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000010000',
                target: '00000000000000000000000000000000000000000000009a110e000000000000',
        )
        expect('getwork', '[]', response)
        assert client.getWork() == work
    }

    @Test
    void getWorkWithParameter() {
        expect('getwork', '["test-block"]', 'true')
        assert client.getWork('test-block') == true
    }

    @Test
    void listReceivedByAddress() {
        def response = '''\
[
    {
        "address" : "1Q5FWXT24wwfkJrUdDzbRwgnKpzqzzY35",
        "account" : "Test1",
        "label" : "Test1",
        "amount" : 153.00000000,
        "confirmations" : 30721
    },
    {
        "address" : "1ZksZYXA42TnStF2hKCnB8zdi7XpfniAH",
        "account" : "Test2",
        "label" : "Test2",
        "amount" : 63.38333333,
        "confirmations" : 24151
    }
]'''
        def addressInfo = [
                new AddressInfo('1Q5FWXT24wwfkJrUdDzbRwgnKpzqzzY35', 'Test1', 153.00000000, 30721),
                new AddressInfo('1ZksZYXA42TnStF2hKCnB8zdi7XpfniAH', 'Test2', 63.38333333, 24151)
        ]
        expect('listreceivedbyaddress', '[10,false]', response)
        assert client.listReceivedByAddress(10, false) == addressInfo
    }

    @Test
    void listReceivedByAddressWhenEmptyResult() {
        expect('listreceivedbyaddress', '[5,true]', '[]')
        assert client.listReceivedByAddress(5, true) == []
    }

    @Test
    void listReceivedByAccount() {
        def response = '''\
[
    {
        "account" : "Test3",
        "label" : "Test3",
        "amount" : 131.77571887,
        "confirmations" : 20224
    },
    {
        "account" : "Test4",
        "label" : "Test4",
        "amount" : 66.38333333,
        "confirmations" : 24153
    }
]'''
        def accountInfo = [
                new AccountInfo('Test3', 131.77571887, 20224),
                new AccountInfo('Test4', 66.38333333, 24153)
        ]
        expect('listreceivedbyaccount', '[11,true]', response)
        assert client.listReceivedByAccount(11, true) == accountInfo
    }

    @Test
    void listReceivedByAccountWhenEmptyResult() {
        expect('listreceivedbyaccount', '[11,true]', '[]')
        assert client.listReceivedByAccount(11, true) == []
    }

    @Test
    void getTransaction() {
        def response = '''\
{
    "amount" : -0.10000000,
    "fee" : -0.00001000,
    "confirmations" : 4768,
    "txid" : "4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2",
    "time" : 1318357443,
    "details" : [
        {
            "account" : "",
            "address" : "14HSLBMexx91o5VuZ77UNrojBiHupYk6PG",
            "category" : "send",
            "amount" : -0.10000000,
            "fee" : -0.00001000
        }
    ]
}'''
        def transaction = new TransactionInfo(
                category: null,
                amount: -0.10000000,
                fee: -0.00001000,
                confirmations: 4768,
                txId: '4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2')

        expect('gettransaction', '["4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2"]', response)
        assert client.getTransaction('4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2') == transaction
    }

    @Test
    void listTransactions() {
        def response = '''\
[
    {
        "account" : "",
        "address" : "14HSLBMexx91o5VuZ77UNrojBiHupYk6PG",
        "category" : "send",
        "amount" : -0.10000000,
        "fee" : -0.00001000,
        "confirmations" : 4895,
        "txid" : "4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2",
        "time" : 1318357443
    },
    {
        "account" : "",
        "address" : "15QQzfnVNnTxU4yGRd1CymQU7P6eEUNyZN",
        "category" : "receive",
        "amount" : 0.09950000,
        "confirmations" : 4892,
        "txid" : "1f183f81b9465171dbf89e8403da8e8143ce1f444d6b5a416872021c91271101",
        "time" : 1318358760
    }
]'''
        def transactions = [
                new TransactionInfo(
                        category: 'send',
                        amount: -0.10000000,
                        fee: -0.00001000,
                        confirmations: 4895,
                        txId: '4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2'
                ),
                new TransactionInfo(
                        category: 'receive',
                        amount: 0.09950000,
                        confirmations: 4892,
                        txId: '1f183f81b9465171dbf89e8403da8e8143ce1f444d6b5a416872021c91271101'
                )
        ]

        expect('listtransactions', '["",2]', response)
        assert client.listTransactions(null, 2) == transactions
    }

    @Test
    void getServerInfo() {
        def response = '''\
{
    "version" : 40000,
    "balance" : 117.47401283,
    "blocks" : 152785,
    "connections" : 2,
    "proxy" : "proxy-test",
    "generate" : true,
    "genproclimit" : -1,
    "difficulty" : 1203461.92637997,
    "hashespersec" : 0,
    "testnet" : true,
    "keypoololdest" : 1300875359,
    "keypoolsize" : 101,
    "paytxfee" : 0.00001000,
    "errors" : ""
}'''
        def info = new ServerInfo('40000', 117.47401283, 152785, 2, 'proxy-test', true, -1, 1203461.92637997, 0, true)
        expect('getinfo', '[]', response)
        assert client.getServerInfo() == info
    }

    @Test
    void sendToAddress() {
        def txid = '4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2'
        def request = '["14HSLBMexx91o5VuZ77UNrojBiHupYk6PG",123456.87654321,"comment","commentto"]'
        expect('sendtoaddress', request, "\"${txid}\"")

        assert client.sendToAddress('14HSLBMexx91o5VuZ77UNrojBiHupYk6PG', 123456.87654321, 'comment', 'commentto') == txid
    }

    @Test
    void sendFrom() {
        def txid = '4c1e830eb17afb766ba5ebe101de79126e18243eb2f97bf5975c286f466d92a2'
        def request = '["account","14HSLBMexx91o5VuZ77UNrojBiHupYk6PG",1234.098765,2,"comment","comment2"]'
        expect('sendfrom', request, "\"${txid}\"")

        assert client.sendFrom('account', '14HSLBMexx91o5VuZ77UNrojBiHupYk6PG', 1234.098765, 2, 'comment', 'comment2') == txid
    }

    @Test
    void stop() {
        expect('stop', '[]')
        client.stop()
    }

    @Test
    void help() {
        expect('help', '["stop"]', '"help-result"')
        assert client.help('stop') == 'help-result'
    }

    @Test
    void move() {
        //move(String fromAccount, String toAccount, double amount, int minimumConfirmations, String comment)
        expect('move', '["from","to",65432.09876543,22,"comment"]', 'true')
        assert client.move('from', 'to', 65432.09876543, 22, 'comment') == true
    }

    @Test
    void moveWhenFromAndToIsNull() {
        //move(String fromAccount, String toAccount, double amount, int minimumConfirmations, String comment)
        expect('move', '["","",65432.09876543,22,"comment"]', 'false')
        assert client.move(null, null, 65432.09876543, 22, 'comment') == false
    }

    @Test
    void validateAddress() {
        def response = '''\
{
    "isvalid" : true,
    "address" : "14HSLBMexx91o5VuZ77UNrojBiHupYk6PG",
    "ismine" : true,
    "account" : "test"
}'''
        def info = new ValidatedAddressInfo(
                isValid: true,
                address: '14HSLBMexx91o5VuZ77UNrojBiHupYk6PG',
                isMine: true,
                account: 'test'
        )
        expect('validateaddress', '["14HSLBMexx91o5VuZ77UNrojBiHupYk6PG"]', response)
        assert client.validateAddress('14HSLBMexx91o5VuZ77UNrojBiHupYk6PG') == info
    }

    @Test
    void validateAddressNotMine() {
        def response = '''\
{
    "isvalid" : true,
    "address" : "1E35mDRzCTtJpAZmRjiiGCAVpAkmKiCR69",
    "ismine" : false
}'''

        def info = new ValidatedAddressInfo(
                isValid: true,
                address: '1E35mDRzCTtJpAZmRjiiGCAVpAkmKiCR69',
                isMine: false,
        )
        expect('validateaddress', '["1E35mDRzCTtJpAZmRjiiGCAVpAkmKiCR69"]', response)
        assert client.validateAddress('1E35mDRzCTtJpAZmRjiiGCAVpAkmKiCR69') == info
    }

    @Test
    void validateAddressNotValid() {
        def response = '{"isvalid" : false,}'
        def info = new ValidatedAddressInfo(isValid: false)
        expect('validateaddress', '["1E35mDRzCTtJpAZmRjiiGCAVpAkmKiCR68"]', response)

        assert client.validateAddress('1E35mDRzCTtJpAZmRjiiGCAVpAkmKiCR68') == info
    }

    @Test
    void backupWallet() {
        expect('backupwallet', '["test-location"]')
        client.backupWallet('test-location')
    }
}